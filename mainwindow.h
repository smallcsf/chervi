#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QObject>
#include "grafics.h"
#include <QDir>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);
//    void paintEvent(QPaintEvent *event);

private:
    Ui::MainWindow *ui;
//    QPixmap *grafic;

private slots:
    void GetTest(int i, int t, int v);

    void on_puBuStatus_clicked();
    void on_puBuMtrxsToOne_clicked();
    void on_puBuMatrxsToOne_2_clicked();
    void on_puBuMatrxsToOne_1_clicked();
    void on_puBUDatasFs_clicked();
    void on_puBuManyFStat_clicked();
    void on_spBxmxRv_valueChanged(int );
    void on_spBxmxRt_valueChanged(int );
    void on_puBUFDmRts_clicked();
    void on_puBUFD_clicked();
    void on_puBustat_clicked();
    void on_Chervi_level(int level);

    void on_ChkBXDatasFsSaveData_clicked(bool checked);
    void on_puBUDatasFsCinfo_clicked();
    void on_puBuGetMedCinfo_clicked();
    void on_puBuAllCinfo_clicked();
    void on_puBuFlilewinds_clicked();
    void on_puBuObrCG_clicked();
    void on_puBuObrCGFileMask_clicked();
    void on_chkBxMax_1_clicked(bool checked);
    void on_chkBxMax_2_clicked();
    void on_chkBxMin_1_clicked();
    void on_chkBxMin_2_clicked();
    void on_puBuAltFilemxRvRtDir_clicked();
    void on_chkBxMaxs_clicked();
    void on_chkBxMins_clicked();
    void on_chkBxAlls_clicked();
    void on_chkBXWinWave_clicked();
    void on_chkBXWinMorlet_clicked();
    void on_puBUDatasFsPrks_clicked();
    void on_puBulsWDatasFPrksClear_clicked();
    void on_puBuManyFPrksStat_clicked();
    void on_puBuAltFilemxRvRtDirPrks_clicked();
    void on_puBuAltFilemxRvRtDirPrksDiap_clicked();
    void on_chkBXWinMorletPrks_clicked();
    void on_chkBXWinWavePrks_clicked();
    void on_chkBxPrksKrai_clicked();
    void on_chkBxPrksAllDlin_clicked();
    void on_chkBxPrksRazriv_clicked();
    void on_pushButtonDirDrawGraphiStr_clicked();
    void on_pushButtonDoDrawGraphiStr_clicked();
    void on_pushButtonDefDrawGraphiStr_clicked();
};

#endif // MAINWINDOW_H

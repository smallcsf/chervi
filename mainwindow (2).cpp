#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "chervi.h"
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->lEAdr->setText("data.mtx");
//    connect (Chervi,SIGNAL(Chervi::LevelDone(int)),this,SLOT(on_Chervi_level(int)));
//    ui->lEAdr->setText(QCoreApplication::applicationDirPath()+"/data.mtx");
//    grafic = new QPixmap(1,1);
//    grafic = new QPixmap(500,500);
//    QPainter pp(grafic);
//    pp.fillRect(QRect(0,0,500,500),QColor(255, 0, 0));
//    pp.drawText(10,10,"gsbcsdgcfbsdfvbgsduc");
//    pp.end();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::on_Chervi_level(int level){
    switch (level){
        case 0: ui->lbLoadStatus->setText("File Loading, wait...");
        case 1: ui->lbLoadStatus->setText("File Loaded, find extrems, wait...");
        case 2: ui->lbLoadStatus->setText("Extrems find, start klasters, wait..."); 
        case 3: ui->lbLoadStatus->setText("Done");
    };
//    QCoreApplication::processEvents();
};

//void MainWindow::paintEvent(QPaintEvent *event){
//    QPainter pain(this);
//    QColor smth(QColor::Hsv);
//    smth.setHsv(10,1,1);
//    pain.fillRect(0,0,300,300,QColor::fromHsv(300,255,255,200));
////            if (grafic->width()!=1) pain.drawPixmap(10,100,*grafic);
//    pain.end();
//};

void MainWindow::on_puBustat_clicked()
{
//    QCoreApplication::processEvents();
//    connect(&mychrvs,SIGNAL(LevelDone(int)),this,SLOT(on_Chervi_level(int)),Qt::DirectConnection);
//    mychrvs.UseF(ui->lEAdr->text(),ui->lEdmxRt->text().toInt(),ui->lEdmxRv->text().toInt(),ui->lEdmxj->text().toInt());
    if  (QFile::exists(ui->lEAdr->text())){
//        Chervi mychrvs(ui->lEAdr->text(),ui->lEdAdrmRts->text(),ui->lEdmxRt->text().toInt(),ui->lEdmxRv->text().toInt(),ui->lEdmxj->text().toInt());
        Chervi mychrvs;

        mychrvs.setmxS(ui->lEdmxRt->text().toInt(),ui->lEdmxRv->text().toInt(),ui->lEdmxj->text().toInt());

        if ( (ui->chkBxmxRts->isChecked())&&(QFile::exists(ui->lEdAdrmRts->text()))) mychrvs.UseFmxRts(ui->lEdAdrmRts->text());

        mychrvs.UseF(ui->lEAdr->text());
        if (mychrvs.check()){
            ui->lbLoadStatus->setText("File Loaded");
            if (ui->ChkBxPreGraf->isChecked()) {
                grafics *mias = new grafics(mychrvs.DrawPreRes(ui->lEAdr->text()+"_Pre"));
                mias->show();
            };
            if (ui->ChkBXPreSaveGraf->isChecked()) mychrvs.DrawPreRes(ui->lEAdr->text()+"_Pre");
            if (ui->ChkBXSaveGraf->isChecked()) mychrvs.DrawRes(ui->lEAdr->text()+"_Res");
            if (ui->ChkBXSaveChastGraf->isChecked()) mychrvs.DrawChastRes(ui->lEAdr->text()+"_ChastRes");
            if (ui->ChkBXSaveData->isChecked()) mychrvs.SaveCepiInfo(ui->lEAdr->text()+"_Cinfo");
//            {
//                QVector < indxs > vooot;
//                vooot.resize(6);
//                vooot[0].Mi=2;
//                vooot[0].Mj=10;
//                vooot[1].Mi=11;
//                vooot[1].Mj=50;
//                vooot[2].Mi=51;
//                vooot[2].Mj=100;
//                vooot[3].Mi=101;
//                vooot[3].Mj=250;
//                vooot[4].Mi=251;
//                vooot[4].Mj=500;
//                vooot[5].Mi=501;
//                vooot[5].Mj=1023;
//
//                mychrvs.statist("dl_cepi.csv",vooot,5);
//            };
        } else ui->lbLoadStatus->setText("smth wrong");
    } else ui->lbLoadStatus->setText("File not found");

//    grafics *myas = new grafics(mychrvs.DrawPreRes());
//    myas->show();
//    MainWindow::paintEvent(0);

};


void MainWindow::on_puBUFD_clicked()
{
    ui->lEAdr->setText(QFileDialog::getOpenFileName(this));
}

void MainWindow::on_puBUFDmRts_clicked()
{
    ui->lEdAdrmRts->setText(QFileDialog::getOpenFileName(this));
}

void MainWindow::on_spBxmxRt_valueChanged(int )
{
    ui->tbWmxRt->setRowCount(ui->spBxmxRt->value());
//    if (ui->tbWmxRt->item( ui->tbWmxRt->rowCount()-1  , 0)->text()=="") ;
////        ui->tbWmxRt->item(ui->tbWmxRt->rowCount()-1,0)->setText("0");
};

void MainWindow::on_spBxmxRv_valueChanged(int )
{
    ui->tbWmxRv->setRowCount(ui->spBxmxRv->value());
}

void MainWindow::on_puBuManyFStat_clicked()
{
    QStringList Adrs;

    sposob th_sp=mins;
    if (ui->chkBxMaxs->isChecked()) th_sp=maxs;
    else if (ui->chkBxMins->isChecked()) th_sp=mins;
    else if (ui->chkBxAlls->isChecked()) th_sp=alls;

    for (int i=0; i < ui->lsWDatasFs->count();++i){
        if ( QFile::exists(ui->lsWDatasFs->item(i)->text()) ){
            for (int j=0; j<ui->tbWmxRt->rowCount(); ++j)
                for (int k=0; k<ui->tbWmxRv->rowCount(); ++k) {
                    Chervi mychrvs;
                    mychrvs.setmxS(ui->tbWmxRt->item(j,0)->text().toInt(),ui->tbWmxRv->item(k,0)->text().toInt(),0);

                    if (ui->ChkBXUseAlt->isChecked())
                    {
                        mychrvs.classic_sp=false;
                        mychrvs.UseFmxRvRts(ui->lEdAltFilemxRvRt->text());
                        mychrvs.setRNode(ui->lEdAltRvt->text().toStdString());
                    }

                    mychrvs.UseF(ui->lsWDatasFs->item(i)->text(),th_sp);
                    if (ui->ChkBXDatasFsPreSaveGraf->isChecked()) mychrvs.DrawPreRes(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_PreRes");
                    if (ui->ChkBXDatasFsSaveGraf->isChecked()) mychrvs.DrawRes(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_Res");
                    if (ui->ChkBXDatasFsSaveChastGraf->isChecked()) mychrvs.DrawChastRes(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_ChastRes");
                    if (ui->ChkBXDatasFsSaveData->isChecked()) mychrvs.SaveCepiInfo(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_Cinfo",
                                                                                    ui->lEdWinMinL->text().toInt(), ui->lEdWinMaxL->text().toInt(),ui->lEdWinSt_t->text().toInt(),ui->lEdWinEnd_t->text().toInt(),ui->lEdWinSt_v->text().toInt(),
                                                                                    ui->lEdWinEnd_v->text().toInt(), !ui->ChkBXDatasFsSaveAllOtnKoordV->isChecked(), !ui->ChkBXDatasFsSaveAllOtnKoordT->isChecked());
                    if (ui->ChkBXSaveDataAllCinfo->isChecked()) Adrs.push_back(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_Cinfo.csv");
                    if (ui->ChkBXDatasFsSaveAll->isChecked())
                    {
                        if (ui->ChkBXDatasFsSaveAllTransp->isChecked())
                            mychrvs.SaveWindowH(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_AllDatasH",
                                                ui->lEdWinMinL->text().toInt(), ui->lEdWinMaxL->text().toInt(),ui->lEdWinSt_t->text().toInt(),ui->lEdWinEnd_t->text().toInt(),ui->lEdWinSt_v->text().toInt(),
                                                ui->lEdWinEnd_v->text().toInt(), !ui->ChkBXDatasFsSaveAllOtnKoordV->isChecked(), !ui->ChkBXDatasFsSaveAllOtnKoordT->isChecked() );
                        else
                            mychrvs.SaveWindowV(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_AllDatasV",
                                                ui->lEdWinMinL->text().toInt(), ui->lEdWinMaxL->text().toInt(), ui->lEdWinSt_t->text().toInt(),ui->lEdWinEnd_t->text().toInt(),ui->lEdWinSt_v->text().toInt(),
                                                ui->lEdWinEnd_v->text().toInt(), !ui->ChkBXDatasFsSaveAllOtnKoordV->isChecked(), !ui->ChkBXDatasFsSaveAllOtnKoordT->isChecked() );
                    }
                    if (ui->ChkBXSaveDataGorbAnaliz->isChecked())
                    {
                        mychrvs.SaveMidCls(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_Ginfo_VGorb");
                        mychrvs.SaveMidCls(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_Ginfo_W2Gorb",
                                           0, -1, 0, -1, 0, -1, false);
                    }
                    if ( (ui->ChkBXFileWinds->isChecked())&&(QFile::exists(ui->lEdFileWinds->text())) )
                    {
                        QVector < wind > winds = Chervi::Dowinds(ui->lEdFileWinds->text());
                        for (int l=0;l<winds.size();++l)
                        {
                            if (ui->ChkBXDatasFsSaveData->isChecked()) mychrvs.SaveCepiInfo(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_window("+QString::number(l)+")_"+"_Cinfo",
                                                                                            winds[l].min_len, winds[l].max_len, winds[l].st_t, winds[l].end_t, winds[l].st_v, winds[l].end_v,
                                                                                            !ui->ChkBXDatasFsSaveAllOtnKoordV->isChecked(), !ui->ChkBXDatasFsSaveAllOtnKoordT->isChecked() );
                            if (ui->ChkBXDatasFsSaveAllTransp->isChecked())
                                mychrvs.SaveWindowH(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_window("+QString::number(l)+")_"+"_AllDatasH",
                                                    winds[l].min_len, winds[l].max_len, winds[l].st_t, winds[l].end_t, winds[l].st_v, winds[l].end_v,
                                                    !ui->ChkBXDatasFsSaveAllOtnKoordV->isChecked(), !ui->ChkBXDatasFsSaveAllOtnKoordT->isChecked() );
                            else
                                mychrvs.SaveWindowV(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_window("+QString::number(l)+")_"+"_AllDatasH",
                                                    winds[l].min_len, winds[l].max_len, winds[l].st_t, winds[l].end_t, winds[l].st_v, winds[l].end_v,
                                                    !ui->ChkBXDatasFsSaveAllOtnKoordV->isChecked(), !ui->ChkBXDatasFsSaveAllOtnKoordT->isChecked() );
                            if (ui->ChkBXSaveDataGorbAnaliz->isChecked())
                            {
                                mychrvs.SaveMidCls(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_window("+QString::number(l)+")_"+"_Ginfo_VGorb",
                                                                                             winds[l].min_len, winds[l].max_len, winds[l].st_t, winds[l].end_t, winds[l].st_v, winds[l].end_v );
                                mychrvs.SaveMidCls(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_window("+QString::number(l)+")_"+"_Ginfo_W2Gorb",
                                                                                             winds[l].min_len, winds[l].max_len, winds[l].st_t, winds[l].end_t, winds[l].st_v, winds[l].end_v , false);
                            }

                        }
                    }

                    if ((ui->chkBXWinWave->isChecked())||(ui->chkBXWinMorlet->isChecked()))
                    {
                        wind m_win;

                        m_win.max_len=-1;
                        m_win.min_len=0;
                        m_win.st_t=0;
                        m_win.st_v=0;

                        if (ui->chkBXWinWave->isChecked())
                        {
                            m_win.end_v=-2;
                            m_win.end_t=-2;
                        } else
                        {
                            m_win.end_v=-3;
                            m_win.end_t=-3;
                        }

                        if (ui->ChkBXDatasFsSaveData->isChecked()) mychrvs.SaveCepiInfo(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_window("+QString::number(m_win.end_v)+")_"+"_Cinfo",
                                                                                        m_win.min_len, m_win.max_len, m_win.st_t, m_win.end_t, m_win.st_v, m_win.end_v,
                                                                                        !ui->ChkBXDatasFsSaveAllOtnKoordV->isChecked(), !ui->ChkBXDatasFsSaveAllOtnKoordT->isChecked() );
                        if (ui->ChkBXDatasFsSaveAllTransp->isChecked())
                            mychrvs.SaveWindowH(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_window("+QString::number(m_win.end_v)+")_"+"_AllDatasH",
                                                m_win.min_len, m_win.max_len, m_win.st_t, m_win.end_t, m_win.st_v, m_win.end_v,
                                                !ui->ChkBXDatasFsSaveAllOtnKoordV->isChecked(), !ui->ChkBXDatasFsSaveAllOtnKoordT->isChecked() );
                        else
                            mychrvs.SaveWindowV(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_window("+QString::number(m_win.end_v)+")_"+"_AllDatasH",
                                                m_win.min_len, m_win.max_len, m_win.st_t, m_win.end_t, m_win.st_v, m_win.end_v,
                                                !ui->ChkBXDatasFsSaveAllOtnKoordV->isChecked(), !ui->ChkBXDatasFsSaveAllOtnKoordT->isChecked() );
                        if (ui->ChkBXSaveDataGorbAnaliz->isChecked())
                        {
                            mychrvs.SaveMidCls(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_window("+QString::number(m_win.end_v)+")_"+"_Ginfo_VGorb",
                                                                                         m_win.min_len, m_win.max_len, m_win.st_t, m_win.end_t, m_win.st_v, m_win.end_v );
                            mychrvs.SaveMidCls(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_window("+QString::number(m_win.end_v)+")_"+"_Ginfo_W2Gorb",
                                                                                         m_win.min_len, m_win.max_len, m_win.st_t, m_win.end_t, m_win.st_v, m_win.end_v , false);
                        }

                        if (ui->ChkBXDatasFsSaveGraf->isChecked()) mychrvs.DrawRes(ui->lsWDatasFs->item(i)->text()+"_"+ui->tbWmxRt->item(j,0)->text()+"_"+ui->tbWmxRv->item(k,0)->text()+ui->lEdDatasFPostfix->text()+"_window("+QString::number(m_win.end_v)+")_"+"_ImgResWin",
                                                                           m_win.min_len, m_win.max_len, m_win.st_t, m_win.end_t, m_win.st_v, m_win.end_v);
                    }

                    if (ui->ChkBXUseAlt->isChecked()) mychrvs.delRNode();
                }
        }
    }
    if (ui->ChkBXSaveDataAllCinfo->isChecked()) Chervi::GetAllCinfo(ui->lEdFAllCinfo->text(),Adrs);
    ui->lbManyFInfo->setText("Ura!!!!!!!!!!");
}

void MainWindow::on_puBUDatasFs_clicked()
{
    QStringList fnames = QFileDialog::getOpenFileNames(this);
    if (fnames.size()>0)
        ui->lsWDatasFs->addItems(fnames);
}


void MainWindow::on_puBuMatrxsToOne_1_clicked()
{
    ui->lEdMtrxsToOne_1->setText(QFileDialog::getOpenFileName(this));
}

void MainWindow::on_puBuMatrxsToOne_2_clicked()
{
    ui->lEdMtrxsToOne_2->setText(QFileDialog::getOpenFileName(this));
}

void MainWindow::on_puBuMtrxsToOne_clicked()
{
    if ( (QFile::exists(ui->lEdMtrxsToOne_1->text()))&&(QFile::exists(ui->lEdMtrxsToOne_2->text())) ){
        Chervi mychrvs;
        sposob sp1=maxs, sp2=maxs;
        if (ui->chkBxMin_1->isChecked()) sp1=mins;
        if (ui->chkBxMin_2->isChecked()) sp2=mins;
        mychrvs.MatrixsToOneF(ui->lEdMtrxsToOne_1->text(),ui->lEdMtrxsToOne_2->text(),sp1,sp2);
        ui->lbMtrxsToOneStatus->setText("Done!");
    };
}

void MainWindow::on_puBuStatus_clicked()
{
    ui->lbLoadStatus->setText("Ready to start");
    ui->lbMtrxsToOneStatus->setText("Ready to start");
    ui->lbManyFInfo->setText("Ready to start");
    ui->lbstatusCinfo->setText("Ready to start");
    ui->lbMtrxsToOneStatus->setText("Ready to start");
    ui->lbPerekrsStatus->setText("Ready to start");
}

void MainWindow::on_ChkBXDatasFsSaveData_clicked(bool checked)
{
    if ( (!checked)&&(ui->ChkBXSaveDataAllCinfo->isChecked()) ) ui->ChkBXSaveDataAllCinfo->setChecked(checked);
}

void MainWindow::on_puBUDatasFsCinfo_clicked()
{
    QStringList fnames = QFileDialog::getOpenFileNames(this);
    if (fnames.size()>0)
        ui->lsWDatasFsCinfo->addItems(fnames);
}

void MainWindow::on_puBuGetMedCinfo_clicked()
{
    for (int i=0; i < ui->lsWDatasFsCinfo->count();++i)
        if ( QFile::exists(ui->lsWDatasFsCinfo->item(i)->text()) ) {
            if (ui->ChkBXDatasFsMedisTCinfo->isChecked())
                Chervi::GetMedThrGrp(ui->lsWDatasFsCinfo->item(i)->text(),ui->lsWDatasFsCinfo->item(i)->text()+"_Medians", 1);
            else Chervi::GetMedThrGrp(ui->lsWDatasFsCinfo->item(i)->text(),ui->lsWDatasFsCinfo->item(i)->text()+"_Medians");
        };
    ui->lbstatusCinfo->setText("Done");
}

void MainWindow::on_puBuAllCinfo_clicked()
{
    QStringList Adrs;
    Adrs.clear();
    for (int i=0;i<ui->lsWDatasFsCinfo->count();++i)
        Adrs.push_back(ui->lsWDatasFsCinfo->item(i)->text());
    Chervi::GetAllCinfo(ui->lEdFAllCinfoOnly->text(),Adrs);
    ui->lbstatusCinfo->setText("Done");
}

void MainWindow::on_puBuFlilewinds_clicked()
{
    QString fname = QFileDialog::getOpenFileName(this);
    ui->lEdFileWinds->setText(fname);
}

void MainWindow::on_puBuObrCG_clicked()
{
    QVector < QString > masks(0);
    QStringList Adrs;
    masks=Chervi::LoadFrmFile(ui->lEdObrCGFileMask->text());
    for (int k=0;k<masks.size();++k)
    {
        Adrs.clear();
        QRegExp rx(masks[k]);
        rx.setPatternSyntax(QRegExp::Wildcard);
        for (int i=0;i<ui->lsWDatasFsCinfo->count();++i)
            if (rx.exactMatch(ui->lsWDatasFsCinfo->item(i)->text()))
                Adrs.push_back(ui->lsWDatasFsCinfo->item(i)->text());
        while (masks[k].indexOf("*")!=-1) masks[k].replace("*", "+");
        Chervi::GetBigInfoCG(ui->lEdObrCGFile->text()+"_"+masks[k]+".CG",Adrs,ui->spBx_c_st->value(),
                             ui->spBx_c_st->value(),ui->spBx_g_st->value(), ui->spBx_g_st->value(),
                             ui->spBx_cp_cls->value(), ui->chBxAltGing->isChecked() );
    }
     ui->lbstatusCinfo->setText("Done");
}

void MainWindow::on_puBuObrCGFileMask_clicked()
{
//    QString str="1234.txt";
//    QRegExp rx("12*.txt");
//    rx.setPatternSyntax(QRegExp::Wildcard);
//    if (rx.exactMatch(str)) ui->lEdObrCGFileMask->setText("true");
//    else ui->lEdObrCGFileMask->setText("false");
    QString fname=QFileDialog::getOpenFileName(this);
    ui->lEdObrCGFileMask->setText(fname);
}


void MainWindow::on_chkBxMax_1_clicked(bool checked)
{
    bool st=ui->chkBxMax_1->isChecked();
    if (ui->chkBxMin_1->isChecked()==st) ui->chkBxMin_1->setChecked(!st);
}

void MainWindow::on_chkBxMax_2_clicked()
{
    bool st=ui->chkBxMax_2->isChecked();
    if (ui->chkBxMin_2->isChecked()==st) ui->chkBxMin_2->setChecked(!st);
}

void MainWindow::on_chkBxMin_1_clicked()
{
    bool st=ui->chkBxMin_1->isChecked();
    if (ui->chkBxMax_1->isChecked()==st) ui->chkBxMax_1->setChecked(!st);
}

void MainWindow::on_chkBxMin_2_clicked()
{
    bool st=ui->chkBxMin_2->isChecked();
    if (ui->chkBxMax_2->isChecked()==st) ui->chkBxMax_2->setChecked(!st);
}

void MainWindow::on_puBuAltFilemxRvRtDir_clicked()
{
    QString fname = QFileDialog::getOpenFileName(this);
    ui->lEdAltFilemxRvRt->setText(fname);
}

void MainWindow::on_chkBxMaxs_clicked()
{
    bool st=ui->chkBxMaxs->isChecked();
    if (st)
    {
        ui->chkBxMins->setChecked(false);
        ui->chkBxAlls->setChecked(false);
    }

    if ((!ui->chkBxMaxs->isChecked())&&
        (!ui->chkBxMins->isChecked())&&
        (!ui->chkBxAlls->isChecked()))
    ui->chkBxMaxs->setChecked(true);
}

void MainWindow::on_chkBxMins_clicked()
{
    bool st=ui->chkBxMins->isChecked();
    if (st)
    {
        ui->chkBxMaxs->setChecked(false);
        ui->chkBxAlls->setChecked(false);
    }

    if ((!ui->chkBxMaxs->isChecked())&&
        (!ui->chkBxMins->isChecked())&&
        (!ui->chkBxAlls->isChecked()))
    ui->chkBxMaxs->setChecked(true);
}

void MainWindow::on_chkBxAlls_clicked()
{
    bool st=ui->chkBxAlls->isChecked();
    if (st)
    {
        ui->chkBxMaxs->setChecked(false);
        ui->chkBxMins->setChecked(false);
    }

    if ((!ui->chkBxMaxs->isChecked())&&
        (!ui->chkBxMins->isChecked())&&
        (!ui->chkBxAlls->isChecked()))
    ui->chkBxMaxs->setChecked(true);
}

void MainWindow::on_chkBXWinWave_clicked()
{
    if (ui->chkBXWinWave->isChecked()) ui->chkBXWinMorlet->setChecked(false);
}

void MainWindow::on_chkBXWinMorlet_clicked()
{
    if (ui->chkBXWinMorlet->isChecked()) ui->chkBXWinWave->setChecked(false);
}

void MainWindow::on_puBUDatasFsPrks_clicked()
{
    QStringList fnames = QFileDialog::getOpenFileNames(this);
    if (fnames.size()>0)
        ui->lsWDatasFsPrks->addItems(fnames);
}

void MainWindow::on_puBulsWDatasFPrksClear_clicked()
{
    ui->lsWDatasFsPrks->clear();
}

void MainWindow::on_puBuManyFPrksStat_clicked()
{
    for (int i=0; i < ui->lsWDatasFsPrks->count();++i)
        if ( QFile::exists(ui->lsWDatasFsPrks->item(i)->text()) )
        {
            QString postfix="";
            Chervi mychrvs;
            wind my_win={0,-1,0,-1,0,-1};
            bool kr_clm=true;

            if (ui->chkBXWinMorletPrks->isChecked())
            {
                my_win.end_t=-3;
                my_win.end_v=-3;
            } else if (ui->chkBXWinWavePrks->isChecked())
            {
                my_win.end_t=-2;
                my_win.end_v=-2;
            }

            if (!ui->chkBxPrksAllDlin->isChecked())
                kr_clm=false;

            if (kr_clm) postfix="_konci_" ;
            else postfix="_celikom_" ;

            postfix=postfix+"_okno("+QString::number(my_win.end_t)+")_";

            mychrvs.setmxS(-1,-1,0);
            mychrvs.classic_sp=false;
            mychrvs.UseFmxRvRts(ui->lEdAltFilemxRvRtPrks->text());
            mychrvs.setRNode(ui->lEdAltRvtPrks->text().toStdString());
            mychrvs.UseF(ui->lsWDatasFsPrks->item(i)->text(),alls);
            mychrvs.SavePrks(ui->lsWDatasFsPrks->item(i)->text()+"_perekr_"+postfix,ui->lEdAltFilemxRvRtPrksDiap->text(),
                             kr_clm,my_win.min_len,my_win.max_len,my_win.st_t,my_win.end_t,my_win.st_v,my_win.end_v);

            mychrvs.delRNode();
        }
    ui->lbPerekrsStatus->setText("Done!");
}

void MainWindow::on_puBuAltFilemxRvRtDirPrks_clicked()
{
    QString fname = QFileDialog::getOpenFileName(this);
    ui->lEdAltFilemxRvRtPrks->setText(fname);
}

void MainWindow::on_puBuAltFilemxRvRtDirPrksDiap_clicked()
{
    QString fname = QFileDialog::getOpenFileName(this);
    ui->lEdAltFilemxRvRtPrksDiap->setText(fname);
}

void MainWindow::GetTest(int i, int t, int v)
{
    ui->lbtest->setText("i="+QString::number(i)+"; t="+QString::number(t)+"; v="+QString::number(v));
}

void MainWindow::on_chkBXWinMorletPrks_clicked()
{
    if (ui->chkBXWinMorletPrks->isChecked()) ui->chkBXWinWavePrks->setChecked(false);
}

void MainWindow::on_chkBXWinWavePrks_clicked()
{
    if (ui->chkBXWinWavePrks->isChecked()) ui->chkBXWinMorletPrks->setChecked(false);
}

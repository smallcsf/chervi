#ifndef CHERVI_H
#define CHERVI_H

#include <QObject>
#include <QtCore>
#include <cmath>
#include <sstream>
#include <QPixmap>
#include <QPainter>
#include <QGraphicsView>
#include <string>

using std::string;
using std::stringstream;

struct indxs{
    int Mi;
    int Mj;
    double w2;
};

struct indxs_mins{
    int Mi;
    int Mj;
    double w2;
    bool stat;
    QVector < int > mins_i;
};

struct mnog{
    QVector < indxs > Ms;
};

struct Rs {
    double Rt;
    double Rv;
};

struct arsMedGrp{
    int t_0;
    int v_n;
    int t_n;
    int dlina;
    int delta_v;
    int delta_t;
    double dl_dt;
};

struct MedGrp {
    int v0;
    QVector < arsMedGrp > VArs;
};

struct Cinf {
    int v_0 ;
    int t_0 ;
    int v_n ;
    int t_n ;
    int dlina ;
    int delta_v ;
    int delta_t ;
    int cls ;
    double w2_0;
    double w2_n;
    double delta_w2;
    int cls_w2;
};

struct wind {
    int min_len;
    int max_len;
    int st_t;
    int end_t;
    int st_v;
    int end_v;
};

struct AmpInf {
    int t;
    int v;
    double w2;
};

struct Rast {
    string val;
    Rast *left;
    Rast *right;
};

enum sposob { maxs, mins , alls} ;

struct Perekr {
    int max_i;
    QVector < indxs_mins > Ms_m;
};

enum spos_prks { krai, vse, razriv } ;

class Chervi : public QObject
{
Q_OBJECT
private:
    QVector < QVector < double > > Datas;
    QVector < mnog > ItogM;
    QVector < Cinf > ItogCinfo;
    QVector < AmpInf > ItogAmp;
    int reset();
    int checkMs(int Mi, int Mj);
    Rs daln(int fmi, int fmj, int smi, int smj);
    int DoMnog();
    int DoAltMnog();
    indxs FSosed(int fmi, int fmj, Rs maxR, int napr=1);
    indxs FSosedAlt(int fmi, int fmj, Rs maxR);
    int FAllSoseds(int ci, int rmi, int rmj);
    int FAllSosedsAlt(int ci, int rmi, int rmj);
    int AddItog(int ci, int mi, int mj);
    Rs mxR;
    int LastMax;
    QColor getRColor(int col);
    int BlizhSosedSleva(int mi, int mj, int bmi, int bmj);
    int BlizhSosedSlevaAlt(int mi, int mj, int bmi, int bmj);
    int Findci(int mi, int mj);
    int lim;
    int med_alls;
    sposob sp;
    QVector < double > mxRts;
    QVector < Rs > mxRvRts;
    double getmxRt(int i);
    double getmxRv(int i);
    double getmxRtAlt(int i);
    double getmxRvAlt(int i);
    double GetOtnKoord(int numb_cep, int numb_ver, bool v, bool real=true);
    int getCGorb(int mi);
    double getW2(int mi, int mj);
    double get_Gorb_w2(int mi);
    bool InWindow(int mi, int min_len=0, int max_len=-1, int st_t=0, int end_t=-1, int st_v=0, int end_v=-1);
    Rast* RNode;
    void AddMins();
    QVector < Perekr > FindPerekr(QString fnameObl, spos_prks sp_prks=krai, int par_rzhr=-1, int min_len=0, int max_len=-1, int st_t=0, int end_t=-1, int st_v=0, int end_v=-1);
    void DrawResPrks(QString fname, QVector < Perekr > prks,int min_len=0, int max_len=-1, int st_t=0, int end_t=-1, int st_v=0, int end_v=-1);

    static QVector < QVector < double > > LoadMtrx(QString fname);
    static int FileType(QString fname);
    static QVector < QVector < QString > > GetInfCinfo(QString fname, int st_n, int end_n);
    static QVector < QVector < QString > > GetCinfGinfo(QString fname, int st_n, int end_n);
    static QVector < QVector < QString > > GetMCepGinfo(QString fname, int cls);
    static QString ClrFstr(QString str);
    static double GetMiddle(QVector < double > ar);
    static void SaveGStolbAlt(QString fname, QVector < QVector < QString > > Ginf);
    static int AddPerekr(QVector < Perekr > &prks, int max_i, int max_Mi, int max_Mj, double max_w2, bool stat, int min_i);
    static void FiltrPerekr(QVector < Perekr > &prks);
protected:
//    QPixmap PixPreRes;

public:
    bool classic_sp;
    explicit Chervi(QObject *parent = 0);
    explicit Chervi(QString fname , QObject *parent = 0);
    explicit Chervi(QString fname ,QString Rtsname , int mxRt, int mxRv, int limit, QObject *parent = 0);
    void SavePrks(QString fname, QString fnameObl, bool use_filtr=false, int par_rzhr=-1,  spos_prks sp_prks=krai, int min_len=0, int max_len=-1, int st_t=0, int end_t=-1, int st_v=0, int end_v=-1);
    void MakeCinf();
    void setmxS(int mxRt, int mxRv, int limit);
    int UseF(QString fname, sposob th_sp = maxs, int next=1);
    int ConvLMaxMin(int next=1);
    QPixmap DrawPreRes(QString fname);
    void DrawGraphiStr(const QString& fname,const int& NumbStr);
    QPixmap DrawRes(QString fname, int min_len=0, int max_len=-1, int st_t=0, int end_t=-1, int st_v=0, int end_v=-1 );
    QPixmap DrawChastRes(QString fname);
    void DrawAllChastRes(QString fname, QColor mycolor, QColor bcolor, QColor alt_color, int create=1);
    int check();
    int UseFmxRts(QString fname);
    int UseFmxRvRts(QString fname);
    void statist( QString fname, QVector < indxs > diap, int minl);
    int SaveCepiInfo(QString fname, int min_len=0, int max_len=-1, int st_t=0, int end_t=-1, int st_v=0, int end_v=-1, bool OtnKoordV=true, bool OtnKoordT=true);
    int MatrixsToOneF(QString fname_1, QString fname_2, sposob sp1, sposob sp2);
    int OverItogM();
    void SaveWindowV(QString fname, int min_len=0, int max_len=-1, int st_t=0, int end_t=-1, int st_v=0, int end_v=-1, bool OtnKoordV=true, bool OtnKoordT=true);
    void SaveWindowH(QString fname, int min_len=0, int max_len=-1, int st_t=0, int end_t=-1, int st_v=0, int end_v=-1, bool OtnKoordV=true, bool OtnKoordT=true);
    void SaveMidCls(QString fname, int min_len=0, int max_len=-1, int st_t=0, int end_t=-1, int st_v=0, int end_v=-1, bool classic=true);
    int getNumcls(QVector < Cinf > vec_cinf, int cls, bool classic=true);

    void delRNode(Rast* PNode = 0);
    void setRNode(string strf, Rast* PNode = 0);
    double CountTree(double t,double v,Rast* PNode = 0);

    static void GetBigInfoCG(QString fname, QStringList Adrs, int c_st_n=-1, int c_end_n=-1, int g_st_n=-1, int g_end_n=-1, int cp_cls=-300, bool alt_ginf=false );
    static int GetMedThrGrp(QString fCepiInfoName, QString fItogName, int wrong_t=0);
    static void GetAllCinfo(QString fname, QStringList Adrs);
    static QVector < wind > Dowinds (QString fname) ;
    static QVector < QString > LoadFrmFile(QString fname) ;
signals:
    void LevelDone(int level);
    void SendTest(int i, int t, int v);

public slots:
    void Doing(int level);

};


double StrToDouble(string str);


#endif // CHERVI_H

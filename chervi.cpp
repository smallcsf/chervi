#include "chervi.h"

Chervi::Chervi(QObject *parent) :
    QObject(parent)
{
    reset();
};

Chervi::Chervi(QString fname , QObject *parent) :
    QObject(parent)
{
    reset();
    UseF(fname);
};

Chervi::Chervi(QString fname ,QString Rtsname, int mxRt, int mxRv, int limit, QObject *parent) :
    QObject(parent)
{
    reset();
    mxR.Rt=mxRt;
    mxR.Rv=mxRv;
    lim=limit;
    UseFmxRts(Rtsname);
    UseF(fname);
};

int Chervi::reset(){
    ItogM.resize(0);
    Datas.resize(0);
    mxRts.resize(0);
    mxRvRts.resize(0);
    ItogAmp.resize(0);
    ItogCinfo.resize(0);
    mxR.Rt=10;
    mxR.Rv=10;
    lim=0;
    med_alls=0;
    sp = maxs;
    RNode=0;
    LastMax=0;
    classic_sp=true;

    connect (this,SIGNAL(LevelDone(int)),this,SLOT(Doing(int)));

    return 0;
};

void Chervi::setmxS(int mxRt, int mxRv, int limit){
    mxR.Rt=mxRt;
    mxR.Rv=mxRv;
    lim=limit;
};

int Chervi::UseF(QString fname, sposob th_sp,int next){
    emit LevelDone(0);
    sp=th_sp;
    ItogM.resize(0);
    Datas.resize(0);
    QFile file(fname);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);
    QStringList strs;
    while (!in.atEnd()){
        strs=ClrFstr(in.readLine()).split(" ");
        Datas.resize(Datas.size()+1);
//        Datas[Datas.size()-1].resize(strs.size());
//        for (int i=0;i<strs.size();++i) Datas[Datas.size()-1][i] = pow( QString(strs.at(i)).toDouble(), 2);
        if ((lim>=strs.size())||(!lim)) Datas[Datas.size()-1].resize(strs.size());
        else Datas[Datas.size()-1].resize(lim);
        for (int i=0;i<Datas[Datas.size()-1].size();++i) Datas[Datas.size()-1][i] = pow( QString(strs.at(i)).toDouble(), 2);
    };
    file.close();

    if (next) emit LevelDone(1);
    return 0;
};

QString Chervi::ClrFstr(QString str){
    str=str.trimmed();
    while (str.indexOf("  ")!=-1) str.replace("  ", " ");

    return str;
};

void Chervi::MakeCinf()
{
    ItogCinfo.resize(0);
    for (int i=0;i<ItogM.size();++i){
        Cinf tmp_cinf = {ItogM[i].Ms[0].Mi, ItogM[i].Ms[0].Mj, ItogM[i].Ms[ItogM[i].Ms.size()-1].Mi, ItogM[i].Ms[ItogM[i].Ms.size()-1].Mj,
                         ItogM[i].Ms.size(), qAbs(ItogM[i].Ms[0].Mi-ItogM[i].Ms[ItogM[i].Ms.size()-1].Mi)+1,
                         qAbs(ItogM[i].Ms[0].Mj-ItogM[i].Ms[ItogM[i].Ms.size()-1].Mj)+1, getCGorb(i),
                         ItogM[i].Ms[0].w2, ItogM[i].Ms[ItogM[i].Ms.size()-1].w2,
                         qAbs(ItogM[i].Ms[0].w2-ItogM[i].Ms[ItogM[i].Ms.size()-1].w2)+1, get_Gorb_w2(i) } ;
        ItogCinfo.push_back(tmp_cinf);
    }
}

int Chervi::SaveCepiInfo(QString fname, int min_len, int max_len, int st_t, int end_t, int st_v, int end_v, bool OtnKoordV, bool OtnKoordT){
    QFile file(fname+".csv");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    ItogCinfo.resize(0);
    out << "v_0 ; t_0 ; v_n ; t_n ; dlina ; delta_v ; delta_t ; Class ; w^2_0; w^2_n ; delta_w^2 ; Class_w^2" << endl;
    for (int i=0;i<ItogM.size();++i){
        Cinf tmp_cinf = {ItogM[i].Ms[0].Mi, ItogM[i].Ms[0].Mj, ItogM[i].Ms[ItogM[i].Ms.size()-1].Mi, ItogM[i].Ms[ItogM[i].Ms.size()-1].Mj,
                   ItogM[i].Ms.size(), qAbs(ItogM[i].Ms[0].Mi-ItogM[i].Ms[ItogM[i].Ms.size()-1].Mi)+1,
                   qAbs(ItogM[i].Ms[0].Mj-ItogM[i].Ms[ItogM[i].Ms.size()-1].Mj)+1, getCGorb(i),
                   ItogM[i].Ms[0].w2, ItogM[i].Ms[ItogM[i].Ms.size()-1].w2,
                   qAbs(ItogM[i].Ms[0].w2-ItogM[i].Ms[ItogM[i].Ms.size()-1].w2)+1, get_Gorb_w2(i) } ;
        ItogCinfo.push_back(tmp_cinf);
        if (InWindow(i, min_len, max_len, st_t, end_t, st_v, end_v))
        {
            out << GetOtnKoord(i,0,true,OtnKoordV) /*tmp_cinf.v_0*/ << ";" << GetOtnKoord(i,0,false,OtnKoordT) /*tmp_cinf.t_0*/ << ";" ;
            out << GetOtnKoord(i,ItogM[i].Ms.size()-1,true,OtnKoordV) /*tmp_cinf.v_n*/ << ";" << GetOtnKoord(i,ItogM[i].Ms.size()-1,false,OtnKoordT) /*tmp_cinf.t_n*/ << ";" ;
            out << tmp_cinf.dlina << ";" ;
            out << tmp_cinf.delta_v << ";" ;
            out << tmp_cinf.delta_t << ";" ;
            out << tmp_cinf.cls << ";" ;
            out << tmp_cinf.w2_0 << ";" ;
            out << tmp_cinf.w2_n << ";" ;
            out << tmp_cinf.delta_w2 << ";" ;
            out << tmp_cinf.cls_w2 << ";" ;
            out << endl;
        }
    };
    file.close();
    return 0;
};

int Chervi::getCGorb(int mi)
{
    int rovno=0,rost=1,spad=-1,vr_gorb=2,vn_gorb=-2, korotko=-15;
    int res=-100;
    int tmp_max=0, tmp_min=0, tmp_gorb=0;
    std::vector< int > min(0);
    std::vector< int > max(0);
    int Bmax=-1, Bmin=-1;
    int v_lkrai=ItogM[mi].Ms[0].Mi;
    int v_rkrai=ItogM[mi].Ms[ItogM[mi].Ms.size()-1].Mi;

    for (int i=1;i<(ItogM[mi].Ms.size()-1);++i)
    {
        if ( (ItogM[mi].Ms[i].Mi>=ItogM[mi].Ms[i-1].Mi)&&(ItogM[mi].Ms[i].Mi>=ItogM[mi].Ms[i+1].Mi)
         &&(ItogM[mi].Ms[i].Mi>v_lkrai)&&(ItogM[mi].Ms[i].Mi>v_rkrai) )
            {
                max.push_back(i);
                if (Bmax==-1) Bmax=0;
                if ( ItogM[mi].Ms[i].Mi>max[Bmax] ) Bmax=max.size()-1;
            }
        if ( (ItogM[mi].Ms[i].Mi<=ItogM[mi].Ms[i-1].Mi)&&(ItogM[mi].Ms[i].Mi<=ItogM[mi].Ms[i+1].Mi)
         &&(ItogM[mi].Ms[i].Mi<v_lkrai)&&(ItogM[mi].Ms[i].Mi<v_rkrai) )
            {
                min.push_back(i);
                if (Bmin==-1) Bmin=0;
                if ( ItogM[mi].Ms[i].Mi<min[Bmin] ) Bmin=min.size()-1;
            }
    }

    if ( (Bmax!=-1)||(Bmin!=-1) )
    { res = -300 ; }

    //if ( (Bmax!=-1)||(Bmin!=-1) )
    //{ int test=1 ;    }

    if (Bmax!=-1) tmp_max = ( (v_lkrai>v_rkrai) ? (ItogM[mi].Ms[max[Bmax]].Mi-v_lkrai) : (ItogM[mi].Ms[max[Bmax]].Mi-v_rkrai) ) ;
    if (Bmin!=-1) tmp_min = ( (v_lkrai<v_rkrai) ? (v_lkrai-ItogM[mi].Ms[min[Bmin]].Mi) : (v_rkrai-ItogM[mi].Ms[min[Bmin]].Mi) ) ;

    if (tmp_max>tmp_min) tmp_gorb=vr_gorb ;
    else if (tmp_max==tmp_min) tmp_gorb=0 ;
    else tmp_gorb=vn_gorb ;

    if (tmp_gorb!=0) res=tmp_gorb;
    else if (v_lkrai!=v_rkrai) res = ( (v_lkrai>v_rkrai) ? spad : rost ) ;
    else res = rovno;

    if (ItogM[mi].Ms.size()<2) res = korotko;

    return res;
}

int Chervi::UseFmxRts(QString fname){
    mxRts.resize(0);
    QFile file(fname);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);
    while (!in.atEnd()){
        mxRts.resize(mxRts.size()+1);
        mxRts[mxRts.size()-1]=in.readLine().toDouble();
    };
    file.close();
    return 0;
};

int Chervi::UseFmxRvRts(QString fname){
    mxRts.resize(0);
    QFile file(fname);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);
    while (!in.atEnd()){
        QStringList strs=ClrFstr(in.readLine()).trimmed().split(" ");
        Rs Rs_tmp = {strs.at(0).toDouble(),strs.at(1).toDouble()};
        mxRvRts.push_back(Rs_tmp);
    };
    file.close();
    return 0;
};

int Chervi::ConvLMaxMin(int next){
    const double pone=0.99999999, mone=-0.99999999;
    for (int j=0;j<Datas[0].size();++j)
        for (int i=1;i<(Datas.size()-1);++i)
            if (Datas[i][j]==1) Datas[i][j]=pone;
            else if (Datas[i][j]==-1) Datas[i][j]=mone;


    for (int j=0;j<Datas[0].size();++j){
        int last=0;
        for (int i=1;i<(Datas.size()-1);++i)
            if (last==0){
                if ( (Datas[i][j]>Datas[i-1][j])&&(Datas[i][j]>Datas[i+1][j]) ) {
                    last=1;
                    if ((sp==maxs)||(sp==alls))
                    {   AmpInf maxxx = { j,i, Datas[i][j] };
                        ItogAmp.push_back(maxxx);
                    }
                    Datas[i][j]=1;
                }else if ( (Datas[i][j]<Datas[i-1][j])&&(Datas[i][j]<Datas[i+1][j]) ){
                    last=-1;
                    if ((sp==mins)||(sp==alls))
                    {
                        AmpInf minnnn = { j,i, Datas[i][j] };
                        ItogAmp.push_back(minnnn);
                    }
                    Datas[i][j]=-1;
                }else if ( (Datas[i][j]>Datas[i-1][j])&&(Datas[i][j]==Datas[i+1][j]) ) {
                    last=2;
                }else if ( (Datas[i][j]<Datas[i-1][j])&&(Datas[i][j]==Datas[i+1][j]) ){
                    last=-2;
                }else if (Datas[i][j]==1) Datas[i][j]=pone;
                else if (Datas[i][j]==-1) Datas[i][j]=mone;
            } else if (last==1){
                if (Datas[i][j]<Datas[i+1][j]){
                    last=-1;
                    if ((sp==mins)||(sp==alls))
                    {
                        AmpInf minnnn = { j,i, Datas[i][j] };
                        ItogAmp.push_back(minnnn);
                    }
                    Datas[i][j]=-1;
                } else if (Datas[i][j]==Datas[i+1][j]) last=-2;
                else if (Datas[i][j]==1) { Datas[i][j]=pone; last=0; }
                else if (Datas[i][j]==-1) { Datas[i][j]=mone; last=0; }
                else last=0;
            } else if (last==-1){
                if (Datas[i][j]>Datas[i+1][j]){
                    last=1;
                    if ((sp==maxs)||(sp==alls))
                    {   AmpInf maxxx = { j,i, Datas[i][j] };
                        ItogAmp.push_back(maxxx);
                    }
                    Datas[i][j]=1;
                } else if (Datas[i][j]==Datas[i+1][j]) last=2;
                else if (Datas[i][j]==1) { Datas[i][j]=pone; last=0; }
                else if (Datas[i][j]==-1) { Datas[i][j]=mone; last=0; }
                else last=0;
            } else if (last==2){
                if (Datas[i][j]>Datas[i+1][j]){
                    last=1;
                    if ((sp==maxs)||(sp==alls))
                    {   AmpInf maxxx = { j,i, Datas[i][j] };
                        ItogAmp.push_back(maxxx);
                    }
                    Datas[i][j]=1;
                    }else if (Datas[i][j]!=Datas[i+1][j]) last=0;
            } else if (last==-2){
                if (Datas[i][j]<Datas[i+1][j]){
                    last=-1;
                    if ((sp==mins)||(sp==alls))
                    {
                        AmpInf minnnn = { j,i, Datas[i][j] };
                        ItogAmp.push_back(minnnn);
                    }
                    Datas[i][j]=-1;
                    }else if (Datas[i][j]!=Datas[i+1][j]) last=0;
            };
         if (Datas[0][j]==1) Datas[0][j]=pone;
         else if (Datas[0][j]==-1) Datas[0][j]=mone;
         if (Datas[Datas.size()-1][j]==1) Datas[Datas.size()-1][j]=pone;
         else if (Datas[Datas.size()-1][j]==-1) Datas[Datas.size()-1][j]=mone;

    };



    if (sp==mins)
    {
        for (int i=0;i<Datas.size();++i)
            for (int j=0;j<Datas[i].size();++j)
                if (Datas[i][j]==-1)  Datas[i][j]=1;
                else Datas[i][j]=0;
    }

//    if (sp==alls)
//    {
//        for (int i=0;i<Datas.size();++i)
//            for (int j=0;j<Datas[i].size();++j)
//                if (Datas[i][j]==-1)  Datas[i][j]=1;
//                else if (Datas[i][j]!=1) Datas[i][j]=0;
//    }

//    for (int i=0;i<Datas.size();++i)
//        for (int j=0;j<Datas[i].size();++j)
//            if ( (Datas[i][j]!=1)&&(Datas[i][j]!=-1) ) Datas[i][j]=0;

    if (next) emit LevelDone(2);
    return 0;
};

void Chervi::AddMins()
{
    for (int i=0;i<Datas.size();++i)
        for (int j=0;j<Datas[i].size();++j)
            if (Datas[i][j]==-1)  Datas[i][j]=1;
            else if (Datas[i][j]==1) Datas[i][j]==-1;
            else Datas[i][j]=0;
}

QPixmap Chervi::DrawPreRes(QString fname){
    QPixmap PixPreRes(Datas[0].size(),Datas.size());
    PixPreRes.fill(QColor(255, 255, 255));
    QPainter mypaint(&PixPreRes);
    for (int j=0;j<Datas[0].size();++j)
        for (int i=0;i<Datas.size();++i)
            if (Datas[i][j]==1) mypaint.drawPoint(j,i);
    mypaint.end();

    PixPreRes.save(fname+".png");

    return PixPreRes;
};


void Chervi::DrawGraphiStr(const QString& fname,const  int& NumbStr)
{
    QPixmap PixPreRes(Datas[0].size(),Datas.size());
    PixPreRes.fill(QColor(255, 255, 255));
    QPainter mypaint(&PixPreRes);

    mypaint.setPen(Qt::green);
    if ( (NumbStr > 0)&&(NumbStr<Datas.size()) )
        for (int j=0;j<Datas[0].size();++j)
            if (
                (!( (j<=((5.5/2.0)*NumbStr))||(j>=(Datas[0].size()-((5.5/2.0)*NumbStr))) ) )
               )
            {
//                mypaint.drawPoint(j,NumbStr);

                for (int k=NumbStr;(k>=0)&&(Datas[k][j] != -1); --k)
                    if ((!( (j<=((5.5/2.0)*k))||(j>=(Datas[0].size()-((5.5/2.0)*k))) ) ))
                        mypaint.drawPoint(j,k);

                for (int k=NumbStr;(k<Datas.size())&&(Datas[k][j] != -1); ++k)
                    if ((!( (j<=((5.5/2.0)*k))||(j>=(Datas[0].size()-((5.5/2.0)*k))) ) ))
                        mypaint.drawPoint(j,k);
            }

    mypaint.setPen(Qt::black);
    for (int i=0;i<PixPreRes.height();++i)
        {
            mypaint.drawPoint((5.5/2.0)*i,i);
            mypaint.drawPoint(PixPreRes.width()-((5.5/2.0)*i),i);
        }

    mypaint.end();

    PixPreRes.save(fname+".png");
};


QPixmap Chervi::DrawRes(QString fname, int min_len, int max_len, int st_t, int end_t, int st_v, int end_v ){
    QPixmap PixRes(Datas[0].size(),Datas.size());
    PixRes.fill(QColor(255, 255, 255));
    QPainter mypaint(&PixRes);
    for (int i=0;i<ItogM.size();++i){
        if (InWindow(i, min_len, max_len, st_t, end_t, st_v, end_v))
        {
            mypaint.setPen(getRColor(i));
            for (int j=0;j<ItogM[i].Ms.size();++j)
                mypaint.drawPoint(ItogM[i].Ms[j].Mj,ItogM[i].Ms[j].Mi);
        }
    };

    mypaint.setPen(Qt::black);
    if ( (end_t==-2)||(end_t==-3)||(end_v==-2)||(end_v==-3) )
    {
        for (int i=0;i<PixRes.height();++i)
            if ((end_t==-2)||(end_v==-2))
            {
                mypaint.drawPoint((8.0*i+1)/2.0,i);
                mypaint.drawPoint(PixRes.width()-(8.0*i+1)/2.0,i);
            } else
            {
                mypaint.drawPoint((5.5/2.0)*i,i);
                mypaint.drawPoint(PixRes.width()-((5.5/2.0)*i),i);
            }


    }
    mypaint.end();

    PixRes.save(fname+".png");

    return PixRes;
};

void Chervi::DrawResPrks(QString fname, QVector < Perekr > prks, int min_len, int max_len, int st_t, int end_t, int st_v, int end_v)
{
    QPixmap PixRes(Datas[0].size(),Datas.size());
    PixRes.fill(QColor(255, 255, 255));
    QPainter mypaint(&PixRes);
    for (int i=0;i<ItogM.size();++i)
        if (InWindow(i,min_len, max_len, st_t, end_t, st_v, end_v))
        {
            if (i<=LastMax) mypaint.setPen(Qt::black);
            else mypaint.setPen(Qt::blue);

            for (int j=0;j<ItogM[i].Ms.size();++j)
                mypaint.drawPoint(ItogM[i].Ms[j].Mj,ItogM[i].Ms[j].Mi);
        }

    mypaint.setPen(Qt::red);
    for (int i=0;i<prks.size();++i)
        for (int j=0;j<prks[i].Ms_m.size();++j)
            mypaint.drawPoint(prks[i].Ms_m[j].Mj,prks[i].Ms_m[j].Mi);

    mypaint.end();

    PixRes.save(fname+".png");
}

QPixmap Chervi::DrawChastRes(QString fname){
    int Ch_diskr = 5000 ;
    double size=Ch_diskr * 1.00 / (4*3.14*2/(6+pow(2+36 , 0.5)));
    QPixmap PixRes(Datas[0].size(),qRound(size));
    PixRes.fill(QColor(255, 255, 255));
    QPainter mypaint(&PixRes);
    for (int i=0;i<ItogM.size();++i){
        mypaint.setPen(getRColor(i));
        for (int j=0;j<ItogM[i].Ms.size();++j)
            mypaint.drawPoint(ItogM[i].Ms[j].Mj, qRound(size) - qRound (Ch_diskr * 1.00 / (4*3.14*(ItogM[i].Ms[j].Mi+1)/(6+pow(2+36 , 0.5))) ));
    };
    mypaint.end();

    PixRes.save(fname+".png");

    return PixRes;
};

QColor Chervi::getRColor(int col){
    return QColor::fromHsv((col*37)%300,255,255,200);
//    switch (col%7){
//        case 1 : return Qt::red;
//        case 2 : return Qt::green;
//        case 3 : return Qt::blue;
//        case 4 : return Qt::cyan;
//        case 5 : return Qt::magenta;
//        case 6 : return Qt::yellow;
//        case 0 : return Qt::gray;
//        default : return Qt::darkGreen;
//
//    };

};

int Chervi::check(){
    int res=1;
    if ( (Datas.size()==0) ) res=0;
//    else {
//        for (int i=0;i<Datas.size();++i)
//            for (int j=0;j<Datas[i].size();++j)
//                if ( (Datas[i][j]!=1)&&(Datas[i][j]!=0)&&(Datas[i][j]!=-1) ) res=0;
//    };
    return res;
};

void Chervi::Doing(int level){
    switch (level){
//        case 0: ;
        case 1:
            Chervi::ConvLMaxMin();
            break;
        case 2:
            if (classic_sp)
            {
                Chervi::DoMnog();
                if (sp==alls)
                {
                    LastMax=ItogM.size()-1;
                    AddMins();
                    Chervi::DoMnog();
                }
            }
            else
            {
                Chervi::DoAltMnog();
                if (sp==alls)
                {
                    LastMax=ItogM.size()-1;
                    AddMins();
                    Chervi::DoAltMnog();
                }
            }
            break;
        case 3: ;

    };

};

int Chervi::checkMs(int Mi, int Mj){
    int res=1;
    for (int i=0;i<ItogM.size();++i)
        for (int j=0;(j<ItogM[i].Ms.size())&&(res);++j)
            if ( (ItogM[i].Ms[j].Mi==Mi)&&(ItogM[i].Ms[j].Mj==Mj) ) res=0;
    return res;
};

Rs Chervi::daln(int fmi, int fmj, int smi, int smj){
    Rs res={-1, -1};
    res.Rt=qAbs(smj-fmj);
    res.Rv=qAbs(smi-fmi);
    return res;
};

indxs Chervi::FSosed(int fmi, int fmj, Rs maxR, int napr){
    indxs res = {-1, -1};
    if (napr==1){
        for (int i=0;i<(maxR.Rv)&&(res.Mi==-1);++i)
            for (int j=1;(j<maxR.Rt)&&(res.Mi==-1)&&((fmj+j)<Datas[0].size());++j){
                if ((res.Mi==-1)&&((fmi+i)<Datas.size()))
                   if (Datas[fmi+i][fmj+j]==1)
                      if (checkMs(fmi+i,fmj+j))
                        if (BlizhSosedSleva(fmi+i, fmj+j, fmi, fmj)) {
                            res.Mi=fmi+i;
                            res.Mj=fmj+j;
                        };
                if ((res.Mi==-1)&&(i!=0)&&((fmi-i)>=0))
                    if (Datas[fmi-i][fmj+j]==1)
                        if (checkMs(fmi-i,fmj+j))
                            if (BlizhSosedSleva(fmi-i, fmj+j, fmi, fmj)) {
                                res.Mi=fmi-i;
                                res.Mj=fmj+j;
                            };
            };
    } else if (napr==-1) {
//        for (int i=0;i<(maxR.Rv)&&(res.Mi==-1);++i)
//            for (int j=1;(j<maxR.Rt)&&(res.Mi==-1)&&((fmj-j)>=0);++j){
//                if ((res.Mi==-1)&&((fmi+i)<Datas.size()))
//                    if (Datas[fmi+i][fmj-j]==1)
//                        if (checkMs(fmi+i,fmj-j) ){
//                            res.Mi=fmi+i;
//                            res.Mj=fmj-j;
//                        };
//                if ((res.Mi==-1)&&(i!=0)&&((fmi-i)>=0))
//                    if (Datas[fmi-i][fmj-j]==1)
//                        if (checkMs(fmi-i,fmj-j) ){
//                            res.Mi=fmi-i;
//                            res.Mj=fmj-j;
//                        };
//            };
    };
    return res;
};

int Chervi::DoMnog(){
    for (int j=0;j<Datas[0].size();++j)
        for (int i=0; i<Datas.size();++i)
            if (Datas[i][j]==1)
                if (checkMs(i,j)){
                    int ci=AddItog(-1,i,j);
                    FAllSoseds(ci, i,j);
                };
    emit LevelDone(3);
    return 0;
};

int Chervi::AddItog(int ci, int mi, int mj){
    if (ci==-1) {
        ItogM.resize(ItogM.size()+1);
        ci=ItogM.size()-1;
        ItogM[ci].Ms.resize(1);
        ItogM[ci].Ms[0].Mi=mi;
        ItogM[ci].Ms[0].Mj=mj;
        ItogM[ci].Ms[0].w2=getW2(mi,mj);
    } else {
        ItogM[ci].Ms.resize(ItogM[ci].Ms.size()+1);
        ItogM[ci].Ms[ItogM[ci].Ms.size()-1].Mi=mi;
        ItogM[ci].Ms[ItogM[ci].Ms.size()-1].Mj=mj;
        ItogM[ci].Ms[ItogM[ci].Ms.size()-1].w2=getW2(mi,mj);
    };
    return ci;
};

double Chervi::getW2(int mi, int mj)
{
    double res=-1;
    for (int i=0;(i<ItogAmp.size())&&(res==-1);++i)
        if ( (ItogAmp[i].v==mi)&&(ItogAmp[i].t==mj))
            res=ItogAmp[i].w2;
    return res;
}

int Chervi::FAllSoseds(int ci, int rmi, int rmj){
    Rs t_mR={getmxRt(rmi), getmxRv(rmi)};
    indxs sosed=FSosed(rmi,rmj,t_mR);
    int res=0;
    while (sosed.Mi!=-1){
        AddItog(ci,sosed.Mi,sosed.Mj);
        rmi=sosed.Mi;
        rmj=sosed.Mj;
        t_mR.Rt=getmxRt(rmi);
        t_mR.Rv=getmxRv(rmi);
        ++res;
        sosed=FSosed(rmi,rmj,t_mR);
    };
    return res;
};

int Chervi::BlizhSosedSleva(int mi, int mj, int bmi, int bmj){
    int res=0;
    indxs sosed={-1, -1};
//    sosed=FSosed(mi,mj,mxR,-1);
    for (int i=0;i<(getmxRv(mi))&&(sosed.Mi==-1);++i)
        for (int j=1;(j<getmxRt(mi))&&(sosed.Mi==-1)&&((mj-j)>=0);++j){
            if ((sosed.Mi==-1)&&((mi+i)<Datas.size()))
                if (Datas[mi+i][mj-j]==1)
                    if ( (Findci(mi+i,mj-j)==-1)||(Findci(mi+i,mj-j)==Findci(bmi,bmj)) ){
                        sosed.Mi=mi+i;
                        sosed.Mj=mj-j;
                    };
            if ((sosed.Mi==-1)&&(i!=0)&&((mi-i)>=0))
                if (Datas[mi-i][mj-j]==1)
                    if ( (Findci(mi-i,mj-j)==-1)||(Findci(mi-i,mj-j)==Findci(bmi,bmj)) ){
                        sosed.Mi=mi-i;
                        sosed.Mj=mj-j;
                    };
        };
    if ( (sosed.Mi==bmi)&&(sosed.Mj==bmj) ) res=1;
    else res=0;

    return res;

};

int Chervi::Findci(int mi, int mj){
    int ci=-1;
    for (int i=0;i<ItogM.size();++i)
        for (int j=0;(j<ItogM[i].Ms.size())&&(ci==-1);++j)
            if ( (ItogM[i].Ms[j].Mi==mi)&&(ItogM[i].Ms[j].Mj==mj) ) ci=i;
    return ci;
};

double Chervi::getmxRt(int i){
    double res=mxR.Rt;
    if (res==-1) res=0.05*i+3;
//    if (i>125) res=0.8*i-90;
//    if ( (i>=0)&&(i<mxRts.size())) res=mxRts[i];
    return res;
};

double Chervi::getmxRv(int i){
    double res=mxR.Rv;
    if (res==-1) res=0.05*i+3;
    return res;
};

int Chervi::GetMedThrGrp(QString fCepiInfoName, QString fItogName, int wrong_t){
    int res=0;
    QVector < MedGrp > ItogCI ;
    ItogCI.resize(0);
    QFile fileCI(fCepiInfoName);
    fileCI.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&fileCI);

    in.readLine(); //заголовок

    while (!in.atEnd()){
        QStringList strs=in.readLine().split(";");

        int v_0=QString(strs.at(0)).toInt();
        int v_ind=-1;

        for (int i=0;(i<ItogCI.size())&&(v_ind==-1);++i)
            if (ItogCI[i].v0==v_0) v_ind=i;

        if (v_ind==-1) {
            ItogCI.resize(ItogCI.size()+1);
            ItogCI[ItogCI.size()-1].v0=v_0;
            ItogCI[ItogCI.size()-1].VArs.resize(1);
            if (wrong_t) ItogCI[ItogCI.size()-1].VArs[0].t_0=QString(strs.at(1)).toInt() ;
            else ItogCI[ItogCI.size()-1].VArs[0].t_0=int(QString(strs.at(1)).toInt()/2+0.7);
            ItogCI[ItogCI.size()-1].VArs[0].v_n=QString(strs.at(2)).toInt();
            if (wrong_t) ItogCI[ItogCI.size()-1].VArs[0].t_n=QString(strs.at(3)).toInt() ;
            else ItogCI[ItogCI.size()-1].VArs[0].t_n=int(QString(strs.at(3)).toInt()/2+0.7);
            ItogCI[ItogCI.size()-1].VArs[0].dlina=QString(strs.at(4)).toInt();
            ItogCI[ItogCI.size()-1].VArs[0].delta_v=QString(strs.at(5)).toInt();
            ItogCI[ItogCI.size()-1].VArs[0].delta_t=ItogCI[ItogCI.size()-1].VArs[0].t_n-ItogCI[ItogCI.size()-1].VArs[0].t_0+1;
            if (ItogCI[ItogCI.size()-1].VArs[0].delta_t)
                ItogCI[ItogCI.size()-1].VArs[0].dl_dt=1.0000*ItogCI[ItogCI.size()-1].VArs[0].dlina/ItogCI[ItogCI.size()-1].VArs[0].delta_t;
            else ItogCI[ItogCI.size()-1].VArs[0].dl_dt=-1;
        } else {
            ItogCI[v_ind].VArs.resize(ItogCI[v_ind].VArs.size()+1);
            if (wrong_t) ItogCI[v_ind].VArs[ItogCI[v_ind].VArs.size()-1].t_0=QString(strs.at(1)).toInt();
            else ItogCI[v_ind].VArs[ItogCI[v_ind].VArs.size()-1].t_0=int(QString(strs.at(1)).toInt()/2+0.7);
            ItogCI[v_ind].VArs[ItogCI[v_ind].VArs.size()-1].v_n=QString(strs.at(2)).toInt();
            if (wrong_t) ItogCI[v_ind].VArs[ItogCI[v_ind].VArs.size()-1].t_n=QString(strs.at(3)).toInt();
            else ItogCI[v_ind].VArs[ItogCI[v_ind].VArs.size()-1].t_n=int(QString(strs.at(3)).toInt()/2+0.7);
            ItogCI[v_ind].VArs[ItogCI[v_ind].VArs.size()-1].dlina=QString(strs.at(4)).toInt();
            ItogCI[v_ind].VArs[ItogCI[v_ind].VArs.size()-1].delta_v=QString(strs.at(5)).toInt();
            ItogCI[v_ind].VArs[ItogCI[v_ind].VArs.size()-1].delta_t=ItogCI[v_ind].VArs[ItogCI[v_ind].VArs.size()-1].t_n-ItogCI[v_ind].VArs[ItogCI[v_ind].VArs.size()-1].t_0+1;
            if (ItogCI[v_ind].VArs[ItogCI[v_ind].VArs.size()-1].delta_t)
                ItogCI[v_ind].VArs[ItogCI[v_ind].VArs.size()-1].dl_dt=1.0000*ItogCI[v_ind].VArs[ItogCI[v_ind].VArs.size()-1].dlina/ItogCI[v_ind].VArs[ItogCI[v_ind].VArs.size()-1].delta_t;
            else ItogCI[v_ind].VArs[ItogCI[v_ind].VArs.size()-1].dl_dt=-1;
        };

    };

    fileCI.close();

    QFile fileItog(fItogName+".csv");
    fileItog.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fileItog);

    out << "v_0;med_t_0;med_v_n;med_t_n;med_dlina;med_delta_v;med_delta_t;med_(dlina/delta_t);"<< endl;

    for (int i=0;i<ItogCI.size();++i){
        QVector < double > myar;
        myar.resize(ItogCI[i].VArs.size());

        out << ItogCI[i].v0 << ";" ;

        //t_0
        for (int j=0;j<myar.size();++j)
            myar[j]=ItogCI[i].VArs[j].t_0;
        out <<  GetMiddle(myar) << ";"  ;

        //v_n
        for (int j=0;j<myar.size();++j)
            myar[j]=ItogCI[i].VArs[j].v_n;
        out << GetMiddle(myar) << ";"  ;

        //t_n
        for (int j=0;j<myar.size();++j)
            myar[j]=ItogCI[i].VArs[j].t_n;
        out << GetMiddle(myar) << ";"  ;

        //dlina
        for (int j=0;j<myar.size();++j)
            myar[j]=ItogCI[i].VArs[j].dlina;
        out << GetMiddle(myar) << ";"  ;

        //delta_v
        for (int j=0;j<myar.size();++j)
            myar[j]=ItogCI[i].VArs[j].delta_v;
        out << GetMiddle(myar) << ";"  ;

        //delta_t
        for (int j=0;j<myar.size();++j)
            myar[j]=ItogCI[i].VArs[j].delta_t;
        out << GetMiddle(myar) << ";"  ;

        //dlina/delta_t
        for (int j=0;j<myar.size();++j)
            myar[j]=ItogCI[i].VArs[j].dl_dt;
        out << GetMiddle(myar) << ";"  ;

        out<< endl;
    };

    fileItog.close();
    return res;
};

double Chervi::GetMiddle(QVector < double > ar){
    double res=-1;
    qSort(ar);
    if (ar.size()%2) res=ar[int(ar.size()/2)];
    else res=(ar[ar.size()/2]+ar[ar.size()/2-1])/2;
    return res;
};

void Chervi::statist( QString fname, QVector < indxs > diap, int minl){
    if ( (diap.size()!=0)&&(ItogM.size()!=0) ) {
        QFile file(fname);
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(&file);
        for (int k=0;k<diap.size();++k){
            out << QString::number(diap[k].Mi) << "-" << QString::number(diap[k].Mj) << ";" ;
            for (int m=0;m<ItogM.size();++m)
                if ( (ItogM[m].Ms[0].Mi>=diap[k].Mi)&&((ItogM[m].Ms[0].Mi<=diap[k].Mj))&&(ItogM[m].Ms.size()>=minl) )
                    out << QString::number(ItogM[m].Ms.size()) << " ; " ;
            out << endl;
        };
    };
};

int Chervi::OverItogM(){
    if (ItogM.size()>0){
        ItogM.resize(0);
        emit LevelDone(2);
    };
    return 0;
};

int Chervi::MatrixsToOneF(QString fname_1, QString fname_2, sposob sp1, sposob sp2){
//    QColor myclr(Qt);

    UseF(fname_1, sp1, 0);
    ConvLMaxMin(0);
    DrawAllChastRes(fname_1, Qt::black , Qt::blue, Qt::red, 1);

    UseF(fname_2, sp2,0);
    ConvLMaxMin(0);
    DrawAllChastRes(fname_1, Qt::blue , Qt::black, Qt::red, 0);

    return 0;
};

void Chervi::DrawAllChastRes(QString fname, QColor mycolor, QColor bcolor, QColor alt_color, int create){
    int Ch_diskr = 5000 ;
    double size=Datas.size()/* * 1.00 / (4*3.14*2/(6+pow(2+36 , 0.5)))*/;
    if (create) {
        QPixmap PixRes(Datas[0].size(),qRound(size));
        PixRes.fill(QColor(255, 255, 255));
        QPainter mypaint(&PixRes);
        mypaint.setPen(mycolor);
        for (int j=0;j<Datas[0].size();++j)
            for (int i=0;i<Datas.size();++i)
                if (Datas[i][j]==1)
                    mypaint.drawPoint(j, i/*qRound(size) - qRound (Ch_diskr/* * 1.00 / (4*3.14*(i+1)/(6+pow(2+36 , 0.5))) )*/);
        mypaint.end();

        PixRes.save(fname+".png");

    } else {
        QImage ImRes(fname+".png");
        QPainter mypaint(&ImRes);
        mypaint.setPen(mycolor);
        for (int j=0;j<Datas[0].size();++j)
            for (int i=0;i<Datas.size();++i)
                if (Datas[i][j]==1){
                    int ni=i/*qRound(size) - qRound (Ch_diskr/* * 1.00 / (4*3.14*(i+1)/(6+pow(2+36 , 0.5))) )*/;
                    int nj=j;
                    if (ImRes.pixel(QPoint(nj, ni))!=ImRes.pixel(QPoint(0, 0))) {
                        mypaint.setPen(alt_color);
                        mypaint.drawPoint(nj, ni);
                        mypaint.setPen(mycolor);
                    } else mypaint.drawPoint(nj, ni);
                };
        mypaint.end();

        ImRes.save(fname+".png");

    };
};

void Chervi::GetAllCinfo(QString fname, QStringList Adrs){
    QVector < QVector < Cinf > > ACinf;
    ACinf.resize(0);
    for (int i=0;i<Adrs.count();++i){
        QFile file(Adrs.at(i));
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream in(&file);
        in.readLine();
        while (!in.atEnd()){
            QStringList inf=in.readLine().split(";");
            int j=inf.at(0).toInt();
            while ( j>=ACinf.size() ) ACinf.resize(ACinf.size()+1);
            Cinf thCinf = { inf.at(0).toInt(), inf.at(1).toInt(), inf.at(2).toInt(), inf.at(3).toInt(), inf.at(4).toInt(), inf.at(5).toInt(), inf.at(6).toInt(), inf.at(7).toInt()   };
            ACinf[j].push_back(thCinf);
        };
        file.close();
    };
    QFile file(fname);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);

    int max_v=0;

    for (int j=0;j<ACinf.size();++j){
        if (ACinf[j].size()) out << "v_0=" << j << ";v_0=" << j << ";v_0=" << j << ";v_0=" << j << ";v_0=" << j << ";v_0=" << j << ";v_0=" << j << ";v_0=" << j << ";" ;
        if (ACinf[j].size()>max_v) max_v=ACinf[j].size();
    };
    out << endl;

    for (int j=0;j<ACinf.size();++j)
        if (ACinf[j].size()) out << "v_0 ; t_0 ; v_n ; t_n ; dlina ; delta_v ; delta_t ; Class ;" ;
    out << endl;


    for (int i=0;i<max_v;++i){
        int c_res=0;
        for (int j=0;j<ACinf.size();++j)
            if (i<ACinf[j].size()){
                out << ACinf[j][i].v_0 << ";" << ACinf[j][i].t_0 << ";" << ACinf[j][i].v_n << ";" << ACinf[j][i].t_n << ";" ;
                out << ACinf[j][i].dlina << ";" << ACinf[j][i].delta_v << ";" << ACinf[j][i].delta_t << ";" << ACinf[j][i].cls << ";" ;
                ++c_res;
            } else if (ACinf[j].size()) out << " ; ; ; ; ; ; ; ;" ;
        if (c_res) out << endl;
    };
    file.close();
};

void Chervi::SaveWindowV(QString fname, int min_len, int max_len, int st_t, int end_t, int st_v, int end_v, bool OtnKoordV, bool OtnKoordT){
    QFile file(fname+".csv");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    if (end_t==-1) end_t=Datas[0].size();
    if (end_v==-1) end_v=Datas.size();
    int maxL=0;
    for (int i=0;i<ItogM.size();++i)
        if (ItogM[i].Ms.size()>maxL) maxL=ItogM[i].Ms.size();

    for (int i=0;i<ItogM.size();++i)
        if (InWindow(i, min_len, max_len, st_t, end_t, st_v, end_v))
            out << "v;t;w2;";
    out << endl ;

    for (int j=0;j<maxL;++j){
        for (int i=0;i<ItogM.size();++i)
            if (InWindow(i, min_len, max_len, st_t, end_t, st_v, end_v)){
                if (ItogM[i].Ms.size()>j){
                    out << QString::number(GetOtnKoord(i,j,true,OtnKoordV)) << " ; " ;
                    out << QString::number(GetOtnKoord(i,j,false,OtnKoordT)) << " ; " ;
                    out << QString::number(ItogM[i].Ms[j].w2) << " ; " ;
                } else {
                    out << "; ; ; " ;
                };
            };
    out << endl ;
    };
    file.close();
};


void Chervi::SaveWindowH(QString fname, int min_len, int max_len, int st_t, int end_t, int st_v, int end_v, bool OtnKoordV, bool OtnKoordT)
{
    QFile file(fname+".csv");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    if (end_t==-1) end_t=Datas[0].size();
    if (end_v==-1) end_v=Datas.size();
    for (int i=0;i<ItogM.size();++i)
    {
        if (InWindow(i, min_len, max_len, st_t, end_t, st_v, end_v))
        {
            out << "v;" ;
            for (int m=0;m<ItogM[i].Ms.size();++m)
                out << QString::number(GetOtnKoord(i,m,true,OtnKoordV)) << ";" ;
            out << endl << "t;" ;
            for (int m=0;m<ItogM[i].Ms.size();++m)
                out << QString::number(GetOtnKoord(i,m,false,OtnKoordT)) << ";" ;
            out << endl << "w2;" ;
            for (int m=0;m<ItogM[i].Ms.size();++m)
                out << QString::number(ItogM[i].Ms[m].w2) << ";" ;
            out << endl;
        }
    }
    file.close();
}


double Chervi::GetOtnKoord(int numb_cep, int numb_ver, bool v, bool real)
{
    if (real)
    {
        if (v) return ItogM[numb_cep].Ms[numb_ver].Mi ;
        else return ItogM[numb_cep].Ms[numb_ver].Mj ;
    } else
    {
        if (v) return ItogM[numb_cep].Ms[numb_ver].Mi-ItogM[numb_cep].Ms[0].Mi ;
        else return ItogM[numb_cep].Ms[numb_ver].Mj-ItogM[numb_cep].Ms[0].Mj ;
    }
}

int Chervi::getNumcls(QVector <Cinf> vec_cinf, int cls, bool classic)
{
    int res=-1;
    for (int i=0;i<vec_cinf.size();++i)
        if ((vec_cinf[i].cls==cls)&&classic) res=i;
        else if ((vec_cinf[i].cls_w2==cls)&&!classic) res=i;
    return res;
}

void Chervi::FiltrPerekr(QVector < Perekr > &prks)
{

}

int Chervi::AddPerekr(QVector < Perekr > &prks, int max_i, int max_Mi, int max_Mj, double max_w2, bool stat, int min_i)
{
    int ci=-1,cj=-1;
    for (int i=0;(i<prks.size())&&(ci==-1);++i)
        if (prks[i].max_i==max_i)
        {
            ci=i;
            for (int j=0;(j<prks[i].Ms_m.size())&&(cj==-1);++j)
                if ((prks[i].Ms_m[j].Mi==max_Mi)&&(prks[i].Ms_m[j].Mj==max_Mj)
                   &&(prks[i].Ms_m[j].w2==max_w2)&&(prks[i].Ms_m[j].stat==stat))
                {
                    cj=j;
                 for (int k=0;(k<prks[i].Ms_m[j].mins_i.size())&&(ci!=-2);++k)
                        if (prks[i].Ms_m[j].mins_i[k]==min_i)
                            ci=-2;
                }
        }

    if (ci==-1)
    {
        indxs_mins mind;
        mind.Mi=max_Mi;
        mind.Mj=max_Mj;
        mind.w2=max_w2;
        mind.stat=stat;
        mind.mins_i.resize(0);
        mind.mins_i.push_back(min_i);

        Perekr pret;
        pret.max_i=max_i;
        pret.Ms_m.resize(0);
        pret.Ms_m.push_back(mind);

        prks.push_back(pret);
    }
    else if ((ci>=0)&&(cj>=0))
        prks[ci].Ms_m[cj].mins_i.push_back(min_i);
    else if ((ci>=0)&&(cj==-1))
    {
        indxs_mins mind;
        mind.Mi=max_Mi;
        mind.Mj=max_Mj;
        mind.w2=max_w2;
        mind.stat=stat;
        mind.mins_i.resize(0);
        mind.mins_i.push_back(min_i);

        prks[ci].Ms_m.push_back(mind);
    }

    return ci;
}

void Chervi::SavePrks(QString fname, QString fnameObl, bool use_filtr, int par_rzhr, spos_prks sp_prks, int min_len, int max_len, int st_t, int end_t, int st_v, int end_v)
{
    QVector < Perekr > prks(0);
    prks=FindPerekr(fnameObl, sp_prks, par_rzhr, min_len, max_len, st_t, end_t, st_v, end_v);
    MakeCinf();

    if (use_filtr) FiltrPerekr(prks);

    QFile file(fname+".csv");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);

    out << " max_i ; max_nachalo ; max_Mi ; max_Mj ; Max_w2 ; max_t_0 ; max_v_0 ; " ;
    out << " max_t_n ; max_v_n ; max_w2_0 ; max_w2_n ; max_delta_t ; " ;
    out << " max_delta_v ; max_delta_w2 ; max_dlina ; max_cls_v ; max_cls_w2 ; " ;
    out << " min_i ; min_t_0 ; min_v_0 ; " ;
    out << " min_t_n ; min_v_n ; min_w2_0 ; min_w2_n ; min_delta_t ; " ;
    out << " min_delta_v ; min_delta_w2 ; min_dlina ; min_cls_v ; min_cls_w2 ; " ;
    out << endl;

    for (int i=0;i<prks.size();++i)
        for (int j=0;j<prks[i].Ms_m.size();++j)
            for (int k=0;k<prks[i].Ms_m[j].mins_i.size();++k)
        {
            out << prks[i].max_i << ";" ;
            if (prks[i].Ms_m[j].stat) out << "1;" ;
            else out << "0;" ;
            out << prks[i].Ms_m[j].Mi << ";" ;
            out << prks[i].Ms_m[j].Mj << ";" ;
            out << prks[i].Ms_m[j].w2 << ";" ;
            out << ItogCinfo[prks[i].max_i].t_0 << ";" ;
            out << ItogCinfo[prks[i].max_i].v_0 << ";" ;
            out << ItogCinfo[prks[i].max_i].t_n << ";" ;
            out << ItogCinfo[prks[i].max_i].v_n << ";" ;
            out << ItogCinfo[prks[i].max_i].w2_0 << ";" ;
            out << ItogCinfo[prks[i].max_i].w2_n << ";" ;
            out << ItogCinfo[prks[i].max_i].delta_t << ";" ;
            out << ItogCinfo[prks[i].max_i].delta_v << ";" ;
            out << ItogCinfo[prks[i].max_i].delta_w2 << ";" ;
            out << ItogCinfo[prks[i].max_i].dlina << ";" ;
            out << ItogCinfo[prks[i].max_i].cls << ";" ;
            out << ItogCinfo[prks[i].max_i].cls_w2 << ";" ;

            out << prks[i].Ms_m[j].mins_i[k] << ";" ;
            out << ItogCinfo[prks[i].Ms_m[j].mins_i[k]].t_0 << ";" ;
            out << ItogCinfo[prks[i].Ms_m[j].mins_i[k]].v_0 << ";" ;
            out << ItogCinfo[prks[i].Ms_m[j].mins_i[k]].t_n << ";" ;
            out << ItogCinfo[prks[i].Ms_m[j].mins_i[k]].v_n << ";" ;
            out << ItogCinfo[prks[i].Ms_m[j].mins_i[k]].w2_0 << ";" ;
            out << ItogCinfo[prks[i].Ms_m[j].mins_i[k]].w2_n << ";" ;
            out << ItogCinfo[prks[i].Ms_m[j].mins_i[k]].delta_t << ";" ;
            out << ItogCinfo[prks[i].Ms_m[j].mins_i[k]].delta_v << ";" ;
            out << ItogCinfo[prks[i].Ms_m[j].mins_i[k]].delta_w2 << ";" ;
            out << ItogCinfo[prks[i].Ms_m[j].mins_i[k]].dlina << ";" ;
            out << ItogCinfo[prks[i].Ms_m[j].mins_i[k]].cls << ";" ;
            out << ItogCinfo[prks[i].Ms_m[j].mins_i[k]].cls_w2 << ";" ;
            out << endl;
        }

    file.close();

    DrawResPrks(fname,prks);
}

QVector < Perekr > Chervi::FindPerekr(QString fnameObl, spos_prks sp_prks, int par_rzhr, int min_len, int max_len, int st_t, int end_t, int st_v, int end_v)
{
    QVector < Perekr > res(0);
    QVector < QVector < double > > obl(0); // t - first, v - second
    obl = LoadMtrx(fnameObl);
    bool lastTry=false;
    for (int i=0;i<=LastMax;++i)
        if (InWindow(i,min_len, max_len, st_t, end_t, st_v, end_v))
            for (int pos=0;(pos<ItogM[i].Ms.size())&&(ItogM[i].Ms.size()>1);)
            {
                int gran_t=0;
                if (ItogM[i].Ms[pos].Mi<obl.size())
                    gran_t=obl[ItogM[i].Ms[pos].Mi][0]/2.0;
                int t_st=(ItogM[i].Ms[pos].Mj-gran_t);
                int t_end=(ItogM[i].Ms[pos].Mj+gran_t);
                lastTry=false;
                for (int t=t_st;(t<=t_end)&&(t<Datas[0].size());++t)
                {
                    if (t<0) t=0;
                    int gran_v=0;
                    if (ItogM[i].Ms[pos].Mi<obl.size())
                        gran_v=obl[ItogM[i].Ms[pos].Mi][1]/2.0;
                    int v_st=(ItogM[i].Ms[pos].Mi-gran_v);
                    int v_end=(ItogM[i].Ms[pos].Mi+gran_v);
                    for (int v=v_st;(v<=v_end)&&(v<Datas.size());++v)
                    {
                        if (v<0) v=0;
                        if ((Datas[v][t]==1)/*||(Datas[v][t]==-1)*/)
                        {
                            int ci=Findci(v,t);
                                if ((ci>LastMax)&&(InWindow(ci,min_len, max_len, st_t, end_t, st_v, end_v)))
                            {
                                if (AddPerekr(res,i,ItogM[i].Ms[pos].Mi,ItogM[i].Ms[pos].Mj,
                                              ItogM[i].Ms[pos].w2,!(pos), ci)>-2)
                                    lastTry=true;
                            }
                        }
                    }
                }
                if (sp_prks==krai)
                    pos+=(ItogM[i].Ms.size()-1);
                else if (sp_prks==vse)
                    ++pos;
                else if (sp_prks==razriv)
                {
                    if (pos!=(ItogM[i].Ms.size()-1))
                    {
                    bool srch=true;

                    if ( (!lastTry)&&(((ItogM[i].Ms[pos+1].Mj-ItogM[i].Ms[pos].Mj)>par_rzhr)
                                   || (qAbs(ItogM[i].Ms[pos+1].Mi-ItogM[i].Ms[pos].Mi)>par_rzhr)) )
                    {
                        ++pos;
                        srch=false;
                    }

                    for (int nxt=pos+1;(srch)&&(nxt<(ItogM[i].Ms.size()-2));++nxt)
                        if ( ((ItogM[i].Ms[nxt+1].Mj-ItogM[i].Ms[nxt].Mj)>par_rzhr)
                             || (qAbs(ItogM[i].Ms[nxt+1].Mi-ItogM[i].Ms[nxt].Mi)>par_rzhr) )
                        {
                            srch=false;
                            pos=nxt;
                        }
                    if (srch) pos=(ItogM[i].Ms.size()-1);
                    }
                    else ++pos;
                }
            }
    return res;
}

void Chervi::SaveMidCls(QString fname, int min_len, int max_len, int st_t, int end_t, int st_v, int end_v, bool classic)
{
    QVector <Cinf> vec_cinf(0);
    QVector < mnog > vec_mdata(0);
    QVector < int > vec_count(0);
    QVector < QVector < int > > vec_cdata(0);
    int max_Mj=0;

    for (int i=0; i<ItogM.size(); ++i)
        for (int j=0;j<ItogM[i].Ms.size();++j)
            if ( (ItogM[i].Ms[j].Mj-ItogM[i].Ms[0].Mj)>max_Mj)
                max_Mj = ItogM[i].Ms[j].Mj - ItogM[i].Ms[0].Mj;


    for (int i=0; i<ItogCinfo.size(); ++i)
    {

        int th_cls=-1;
        if (classic) th_cls=getNumcls(vec_cinf,ItogCinfo[i].cls);
        else th_cls=getNumcls(vec_cinf,ItogCinfo[i].cls_w2, classic);

        if ( (th_cls==-1)&&(ItogM[i].Ms.size()>2)&&(InWindow(i, min_len, max_len, st_t, end_t, st_v, end_v)) )
        {
            vec_cinf.push_back(ItogCinfo[i]);
            vec_cinf[vec_cinf.size()-1].t_n-=vec_cinf[vec_cinf.size()-1].t_0;
            vec_cinf[vec_cinf.size()-1].t_0=0;

            vec_count.push_back(1);

            vec_mdata.resize(vec_mdata.size()+1);
            vec_cdata.resize(vec_cdata.size()+1);
            vec_mdata[vec_mdata.size()-1].Ms.resize(max_Mj+1);
            vec_cdata[vec_cdata.size()-1].resize(max_Mj+1);
            for (int j=0;j<=max_Mj;++j)
            {
                vec_mdata[vec_mdata.size()-1].Ms[j].Mi=-1;
                vec_mdata[vec_mdata.size()-1].Ms[j].Mj=-1;
                vec_mdata[vec_mdata.size()-1].Ms[j].w2=-1;
                vec_cdata[vec_cdata.size()-1][j]=0;
            }
            for (int j=0;j<ItogM[i].Ms.size();++j)
            {
                int Mj=ItogM[i].Ms[j].Mj-ItogM[i].Ms[0].Mj;
                vec_mdata[vec_mdata.size()-1].Ms[Mj].Mj=Mj;
                vec_mdata[vec_mdata.size()-1].Ms[Mj].Mi=ItogM[i].Ms[j].Mi;
                vec_mdata[vec_mdata.size()-1].Ms[Mj].w2=ItogM[i].Ms[j].w2;
                vec_cdata[vec_cdata.size()-1][Mj]=1;
            }
        }
        else if ( (ItogM[i].Ms.size()>2)&&(InWindow(i, min_len, max_len, st_t, end_t, st_v, end_v)) )
        {
            vec_cinf[th_cls].delta_t+=ItogCinfo[i].delta_t;
            vec_cinf[th_cls].delta_v+=ItogCinfo[i].delta_v;
            vec_cinf[th_cls].dlina+=ItogCinfo[i].dlina;
            vec_cinf[th_cls].t_n+=ItogCinfo[i].t_n-ItogCinfo[i].t_0;
            vec_cinf[th_cls].v_0+=ItogCinfo[i].v_0;
            vec_cinf[th_cls].v_n+=ItogCinfo[i].v_n;
            vec_cinf[th_cls].w2_0+=ItogCinfo[i].w2_0;
            vec_cinf[th_cls].w2_n+=ItogCinfo[i].w2_n;
            vec_cinf[th_cls].delta_w2+=ItogCinfo[i].delta_w2;

            vec_count[th_cls]+=1;

            for (int j=0;j<ItogM[i].Ms.size();++j)
            {
                int Mj=ItogM[i].Ms[j].Mj-ItogM[i].Ms[0].Mj;
                if (vec_mdata[th_cls].Ms[Mj].Mj==-1)
                {
                    vec_mdata[th_cls].Ms[Mj].Mj=Mj;
                    vec_mdata[th_cls].Ms[Mj].Mi=ItogM[i].Ms[j].Mi;
                    vec_mdata[th_cls].Ms[Mj].w2=ItogM[i].Ms[j].w2;
                    vec_cdata[th_cls][Mj]=1 ;
                }
                else
                {
                    vec_mdata[th_cls].Ms[Mj].Mi+=ItogM[i].Ms[j].Mi;
                    vec_mdata[th_cls].Ms[Mj].w2+=ItogM[i].Ms[j].w2;
                    vec_cdata[th_cls][Mj]+=1 ;
                }
            }
        }
    }



    for (int i=0;i<vec_cinf.size();++i)
    {
        vec_cinf[i].delta_t/=vec_count[i];
        vec_cinf[i].delta_v/=vec_count[i];
        vec_cinf[i].dlina/=vec_count[i];
        vec_cinf[i].t_n/=vec_count[i];
        vec_cinf[i].v_0/=vec_count[i];
        vec_cinf[i].v_n/=vec_count[i];
        vec_cinf[i].w2_0/=vec_count[i];
        vec_cinf[i].w2_n/=vec_count[i];
        vec_cinf[i].delta_w2/=vec_count[i];

        for (int j=0;j<vec_mdata[i].Ms.size();++j)
        {
            if (vec_mdata[i].Ms[j].Mj!=-1)
            {
                vec_mdata[i].Ms[j].Mi/=vec_cdata[i][j];
                vec_mdata[i].Ms[j].w2/=vec_cdata[i][j];
            }
        }
    }

    QFile file(fname+".csv");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);

    out << "v_0 ; t_0 ; v_n ; t_n ; dlina ; delta_v ; delta_t ; " ;
    if (classic) out << "class_v ; " ;
    else out << "Class_w2 ; " ;
    out << "w2_0 ; w2_n ; delta_w2 ; count ; " ;
    out << endl;

    for (int i=0;i<vec_cinf.size();++i)
    {
        out << vec_cinf[i].v_0 << ";" << vec_cinf[i].t_0 << ";" ;
        out << vec_cinf[i].v_n << ";" << vec_cinf[i].t_n << ";" ;
        out << vec_cinf[i].dlina << ";" << vec_cinf[i].delta_v << ";" ;
        out << vec_cinf[i].delta_t << ";" ;
        if (classic) out << vec_cinf[i].cls << ";" ;
        else out << vec_cinf[i].cls_w2 << ";" ;
        out << vec_cinf[i].w2_0 << ";" ;
        out << vec_cinf[i].w2_n << ";" << vec_cinf[i].delta_w2 << ";" ;
        out << vec_count[i] << ";" ;

        out << endl;
    }

    out << endl;

    for (int i=0;i<vec_mdata.size();++i)
    {
        int cls=-100;
        if (classic) cls=vec_cinf[i].cls;
        else cls=vec_cinf[i].cls_w2;

        out << "class=" << cls << ";" << "v" << ";" ;
        for (int j=0;j<vec_mdata[i].Ms.size();++j)
            if (vec_mdata[i].Ms[j].Mj!=-1)
                out << vec_mdata[i].Ms[j].Mi << ";" ;
        out << endl;
        out << "class=" << cls << ";" << "t" << ";" ;
        for (int j=0;j<vec_mdata[i].Ms.size();++j)
            if (vec_mdata[i].Ms[j].Mj!=-1)
                out << vec_mdata[i].Ms[j].Mj << ";" ;
        out << endl;
        out << "class=" << cls << ";" << "w2" << ";" ;
        for (int j=0;j<vec_mdata[i].Ms.size();++j)
            if (vec_mdata[i].Ms[j].Mj!=-1)
                out << vec_mdata[i].Ms[j].w2 << ";" ;
        out << endl;
    }

    out << endl;


    out << "v_0 ; t_0 ; v_n ; t_n ; dlina ; delta_v ; delta_t ; " ;
    if (classic) out << "class_v ; " ;
    else out << "Class_w2 ; " ;
    out << "w2_0 ; w2_n ; delta_w2 ; count ; " ;
    out << endl;

    for (int i=0;i<vec_cinf.size();++i)
    {
        vec_cinf[i].t_0=0;
        vec_cinf[i].v_0=vec_mdata[i].Ms[0].Mi;
        vec_cinf[i].w2_0=vec_mdata[i].Ms[0].w2;

        int last_Mj=0, dl=0;
        for (int j=0;j<vec_mdata[i].Ms.size();++j)
            if (vec_mdata[i].Ms[j].Mj!=-1)
            {
                ++dl;
                if (j>last_Mj) last_Mj=j;
            }

        vec_cinf[i].t_n=vec_mdata[i].Ms[last_Mj].Mj;
        vec_cinf[i].v_n=vec_mdata[i].Ms[last_Mj].Mi;
        vec_cinf[i].dlina=dl;
        vec_cinf[i].delta_t=vec_cinf[i].t_n;
        vec_cinf[i].delta_v=qAbs(vec_cinf[i].v_n-vec_cinf[i].v_0);
        vec_cinf[i].w2_n=vec_mdata[i].Ms[last_Mj].w2;
        vec_cinf[i].delta_w2=qAbs(vec_cinf[i].w2_n-vec_cinf[i].w2_0);



        out << vec_cinf[i].v_0 << ";" << vec_cinf[i].t_0 << ";" ;
        out << vec_cinf[i].v_n << ";" << vec_cinf[i].t_n << ";" ;
        out << vec_cinf[i].dlina << ";" << vec_cinf[i].delta_v << ";" ;
        out << vec_cinf[i].delta_t << ";" ;
        if (classic) out << vec_cinf[i].cls << ";" ;
        else out << vec_cinf[i].cls_w2 << ";" ;
        out << vec_cinf[i].w2_0 << ";" ;
        out << vec_cinf[i].w2_n << ";" << vec_cinf[i].delta_w2 << ";" ;
        out << vec_count[i] << ";" ;

        out << endl;
    }


    file.close();
}


bool Chervi::InWindow(int mi, int min_len, int max_len, int st_t, int end_t, int st_v, int end_v)
{
// -2 = Wave , -3=Morlet
    bool res=false;

    if ((max_len==-1)||(max_len==-2)||(max_len==-3))
        max_len=ItogM[mi].Ms.size()+1000;
    if (end_t==-1) end_t=ItogM[mi].Ms[ItogM[mi].Ms.size()-1].Mj+1000;
    if (end_v==-1) end_v=ItogM[mi].Ms[ItogM[mi].Ms.size()-1].Mi+1000;

    if ((end_t!=-2)&&(end_v!=-2)&&(end_t!=-3)&&(end_v!=-3))
    {
        if ( (ItogM[mi].Ms.size()>=min_len)&&(ItogM[mi].Ms.size()<=max_len)
          &&(ItogM[mi].Ms[0].Mi>=st_v)&&(ItogM[mi].Ms[0].Mj>=st_t)
          &&(ItogM[mi].Ms[ItogM[mi].Ms.size()-1].Mi<=end_v)
          &&(ItogM[mi].Ms[ItogM[mi].Ms.size()-1].Mj<=end_t) )
            res=true;

        for (int i=0;(i<ItogM[mi].Ms.size())&&(res);++i)
            if ( (ItogM[mi].Ms[i].Mi>end_v)||(ItogM[mi].Ms[i].Mi<st_v) )
                res=false;

    } else if((end_t==-2)||(end_v==-2)||(end_t==-3)||(end_v==-3))
    {
        if ( (ItogM[mi].Ms.size()>=min_len)&&(ItogM[mi].Ms.size()<=max_len))
             res=true;



        for (int i=0;(i<ItogM[mi].Ms.size())&&(res);++i)
        {
            if ( ((end_t==-2)||(end_v==-2))
              &&( (ItogM[mi].Ms[i].Mj<=((8.0*ItogM[mi].Ms[i].Mi+1)/2.0))
              ||(ItogM[mi].Ms[i].Mj>=(Datas[0].size()-((8.0*ItogM[mi].Ms[i].Mi+1)/2.0))) ) )
                res=false;
            else if ( ((end_t==-3)||(end_v==-3))
              &&( ((ItogM[mi].Ms[i].Mj<=((5.5/2.0)*ItogM[mi].Ms[i].Mi)))
              ||(ItogM[mi].Ms[i].Mj>=(Datas[0].size()-((5.5/2.0)*ItogM[mi].Ms[i].Mi))) ) )
                res=false;
        }
    }


    return res;
}

QVector < wind > Chervi::Dowinds (QString fname)
{
    QVector < wind > res(0);

    QFile file(fname);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);

    in.readLine();

    while (!in.atEnd())
    {
        QStringList strs=ClrFstr(in.readLine()).split(" ");
        wind tmp;
        tmp.min_len=strs.at(0).toInt();
        tmp.max_len=strs.at(1).toInt();
        tmp.st_t=strs.at(2).toInt();
        tmp.end_t=strs.at(3).toInt();
        tmp.st_v=strs.at(4).toInt();
        tmp.end_v=strs.at(5).toInt();
        res.push_back(tmp);
    }

    file.close();
    return res;
}

int Chervi::FileType(QString fname)
{
    // 0 - Cinfo, 1 - Ginfo, -1 - Wrong
    int res=-1;
    if ( fname.indexOf("_Cinfo")!=-1 ) res=0;
    if ( fname.indexOf("_Ginfo")!=-1 ) res=1;
    return res ;
}

QVector < QVector < QString > > Chervi::GetInfCinfo(QString fname, int st_n, int end_n)
{
    QVector < QVector < QString > > res(end_n-st_n+1);
    QFile file(fname);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);
    QStringList strs;
    while (!in.atEnd()){
        strs=ClrFstr(in.readLine()).split(";");
        if (strs.size()>1)
            for (int i=(st_n-1);i<end_n;++i)
                res[i-st_n+1].push_back(strs.at(i));
    };
    file.close();

    return res;
}

QVector < QVector < QString > > Chervi::GetCinfGinfo(QString fname, int st_n, int end_n)
{
    QVector < QVector < QString > > res(end_n-st_n+1);
    for (int i=0;i<res.size();++i)
        for (int j=0;j<6;++j)
            res[i].push_back(" ");
    QFile file(fname);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);
    QStringList strs;
    QString str="11111";
    while (str!=""){
        str=in.readLine();
        strs=ClrFstr(str).split(";");
        if ((strs.size()>1)&&(strs.at(0).indexOf("v")==-1))
            for (int i=(st_n-1);i<end_n;++i)
                res[i-st_n+1][strs.at(7).toInt()+3]=strs.at(i);
        else if (strs.at(0).indexOf("v")!=-1)
            for (int i=(st_n-1);i<end_n;++i)
                res[i-st_n+1][0]=strs.at(i);
    };
    file.close();

    return res;
}

QVector < QVector < QString > > Chervi::GetMCepGinfo(QString fname, int cls)
{
    QVector < QVector < QString > > res(3);
    QFile file(fname);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);
    QStringList strs;
    bool t=true;
    while ( (!in.atEnd())&&(t) ){
        strs=ClrFstr(in.readLine()).split(";");
        if (strs.at(0).indexOf("class=")!=-1)
            if (strs.at(0).mid(6,strs.at(0).size()-6).toInt()==cls)
            {
                QStringList strs_t=ClrFstr(in.readLine()).split(";");
                QStringList strs_w2=ClrFstr(in.readLine()).split(";");
                res[0].resize(strs_t.at(strs_t.size()-2).toInt()+1);
                res[1].resize(strs_t.at(strs_t.size()-2).toInt()+1);
                res[2].resize(strs_t.at(strs_t.size()-2).toInt()+1);
                for (int i=0;i<res[0].size();++i)
                {
                    res[0][i]=" ";
                    res[1][i]=" ";
                    res[2][i]=" ";
                }

                for (int i=2;i<strs_t.size()-1;++i)
                {
                    res[2][strs_t.at(i).toInt()]=strs_w2.at(i);
                    res[1][strs_t.at(i).toInt()]=strs_t.at(i);
                    res[0][strs_t.at(i).toInt()]=strs.at(i);
                }

                res[0].push_front(strs.at(1));
                res[0].push_front(strs.at(0));
                res[1].push_front(strs_t.at(1));
                res[1].push_front(strs_t.at(0));
                res[2].push_front(strs_w2.at(1));
                res[2].push_front(strs_w2.at(0));

                t=false;
            }
    };
    file.close();

    return res;
}

void Chervi::GetBigInfoCG(QString fname, QStringList Adrs, int c_st_n, int c_end_n, int g_st_n, int g_end_n, int cp_cls,bool alt_ginf)
{
    QVector < QVector < QString > > Cinf(0), Ginf(0), CpSr(0), tmp(0);
    int max_c=0, max_g=0, max_sr=0;

    for (int i=0;i<Adrs.size();++i)
    {
        int tp=FileType(Adrs.at(i));
        if ( (tp==0)&&(c_st_n!=0) )
        {
            tmp=GetInfCinfo(Adrs.at(i), c_st_n, c_end_n);
            if (tmp.size()>0)
                for (int m=0;m<tmp.size();++m)
                {
                    Cinf.resize(Cinf.size()+1);
                    Cinf[Cinf.size()-1].push_back(Adrs.at(i));
                    for (int n=0;n<tmp[m].size();++n)
                        Cinf[Cinf.size()-1].push_back(tmp[m][n]);
                }
        }

        if ( (tp==1)&&(g_st_n!=0) )
        {
            tmp=GetCinfGinfo(Adrs.at(i), g_st_n, g_end_n);
            if (tmp.size()>0)
                for (int m=0;m<tmp.size();++m)
                {
                    Ginf.resize(Ginf.size()+1);
                    Ginf[Ginf.size()-1].push_back(Adrs.at(i));
                    for (int n=0;n<tmp[m].size();++n)
                        Ginf[Ginf.size()-1].push_back(tmp[m][n]);
                }
        }

        if ( (tp==1)&&(cp_cls!=-3) )
        {
            tmp=GetMCepGinfo(Adrs.at(i), cp_cls);
            if (tmp.size()>0)
                for (int m=0;m<tmp.size();++m)
                {
                    CpSr.resize(CpSr.size()+1);
                    CpSr[CpSr.size()-1].push_back(Adrs.at(i));
                    for (int n=0;n<tmp[m].size();++n)
                        CpSr[CpSr.size()-1].push_back(tmp[m][n]);
                }
        }
    }

    for(int i=0;i<Cinf.size();++i)
        if (Cinf[i].size()>max_c) max_c=Cinf[i].size();

    for(int i=0;i<Ginf.size();++i)
        if (Ginf[i].size()>max_g) max_g=Ginf[i].size();

    for(int i=0;i<CpSr.size();++i)
        if (CpSr[i].size()>max_sr) max_sr=CpSr[i].size();

    QFile file(fname+".csv");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);

    for (int i=0; (i<max_c)||(i<max_g)||(i<(max_sr-1)) ;++i)
    {
        if (Cinf.size()>0)
            for (int j=0;j<Cinf.size();++j)
            {
                if (i<Cinf[j].size())
                    out << Cinf[j][i] ;
                out << ";" ;
            }

        if (Ginf.size()>0)
            for (int j=0;j<Ginf.size();++j)
            {
                if (i<Ginf[j].size())
                    out << Ginf[j][i] ;
                out << ";" ;
            }

        if (CpSr.size()>0)
            for (int j=0;j<CpSr.size();++j)
            {
                int ik=i;
                if (i>=2) ik=i+1;
                if ( (ik<CpSr[j].size())&&(ik!=1) )
                    out << CpSr[j][ik] ;
                else if ( (ik<CpSr[j].size())&&(ik==1) )
                    out << CpSr[j][ik+1] << "_" << CpSr[j][ik];
                out << ";" ;
            }

        out << endl ;
    }


    file.close();

    if (alt_ginf) SaveGStolbAlt(fname+"_alt_ginf",Ginf);
}

QVector < QString > Chervi::LoadFrmFile(QString fname)
{
    QVector < QString > res(0);
    QFile file(fname);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);
    while (!in.atEnd())
    {
        QString str=ClrFstr(in.readLine());
        if (str.size()>0) res.push_back(str);
    }
    file.close();
    return res;
}

double Chervi::get_Gorb_w2(int mi)
{
    int rovno=0,rost=1,spad=-1,vr_gorb=2,vn_gorb=-2, korotko=-15;
    int res=-100;
    int tmp_max=0, tmp_min=0, tmp_gorb=0;
    std::vector< int > min(0);
    std::vector< int > max(0);
    int Bmax=-1, Bmin=-1;
    int v_lkrai=ItogM[mi].Ms[0].w2;
    int v_rkrai=ItogM[mi].Ms[ItogM[mi].Ms.size()-1].w2;

    for (int i=1;i<(ItogM[mi].Ms.size()-1);++i)
    {
        if ( (ItogM[mi].Ms[i].w2>=ItogM[mi].Ms[i-1].w2)&&(ItogM[mi].Ms[i].w2>=ItogM[mi].Ms[i+1].w2)
         &&(ItogM[mi].Ms[i].w2>v_lkrai)&&(ItogM[mi].Ms[i].w2>v_rkrai) )
            {
                max.push_back(i);
                if (Bmax==-1) Bmax=0;
                if ( ItogM[mi].Ms[i].w2>max[Bmax] ) Bmax=max.size()-1;
            }
        if ( (ItogM[mi].Ms[i].w2<=ItogM[mi].Ms[i-1].w2)&&(ItogM[mi].Ms[i].w2<=ItogM[mi].Ms[i+1].w2)
         &&(ItogM[mi].Ms[i].w2<v_lkrai)&&(ItogM[mi].Ms[i].w2<v_rkrai) )
            {
                min.push_back(i);
                if (Bmin==-1) Bmin=0;
                if ( ItogM[mi].Ms[i].w2<min[Bmin] ) Bmin=min.size()-1;
            }
    }

    if ( (Bmax!=-1)||(Bmin!=-1) )
    { res = -300 ; }

    //if ( (Bmax!=-1)||(Bmin!=-1) )
    //{ int test=1 ;    }

    if (Bmax!=-1) tmp_max = ( (v_lkrai>v_rkrai) ? (ItogM[mi].Ms[max[Bmax]].w2-v_lkrai) : (ItogM[mi].Ms[max[Bmax]].w2-v_rkrai) ) ;
    if (Bmin!=-1) tmp_min = ( (v_lkrai<v_rkrai) ? (v_lkrai-ItogM[mi].Ms[min[Bmin]].w2) : (v_rkrai-ItogM[mi].Ms[min[Bmin]].w2) ) ;

    if (tmp_max>tmp_min) tmp_gorb=vr_gorb ;
    else if (tmp_max==tmp_min) tmp_gorb=0 ;
    else tmp_gorb=vn_gorb ;

    if (tmp_gorb!=0) res=tmp_gorb;
    else if (v_lkrai!=v_rkrai) res = ( (v_lkrai>v_rkrai) ? spad : rost ) ;
    else res = rovno;

    if (ItogM[mi].Ms.size()<2) res = korotko;

    return res;
}

void Chervi::SaveGStolbAlt(QString fname, QVector < QVector < QString > > Ginf)
{
    QFile file(fname+".csv");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);

    for (int i=0;i<Ginf.size();++i)
        for (int j=2;j<Ginf[i].size();++j)
        {
            out << Ginf[i][0] << ";" << Ginf[i][j] << ";" << j-4 << ";";
            out << endl;
        }

    file.close();
}

double StrToDouble(string str)
{
    stringstream ss;
    double res = 0;
    ss << str;
    ss >> res;
    return res;
}

void Chervi::setRNode(string strf, Rast* PNode)
{
    if (PNode ==0 ) RNode = new Rast;
    PNode = RNode;
    PNode->left=0;
    PNode->right=0;

    int lev=1, no=-1;
    double prior=100000;

    for (int i=0;i<strf.size();++i)
    {
        switch (strf[i])
        {
            case '*': if ((lev*4.0)<prior)
            {
                no=i;
                prior=lev*4.0;
            }
            case '/': if ((lev*4.0)<prior)
            {
                no=i;
                prior=lev*4.0;
            }
            break;
            case '^': if ((lev*6.0)<prior)
            {
                no=i;
                prior=lev*6.0;
            }
            break;
            case '+': if ((lev*2.0)<prior)
            {
                no=i;
                prior=lev*2.0;
            }
            break;
            case '-': if ((lev*2.0)<prior)
            {
                no=i;
                prior=lev*2.0;
            }
            break;
            case '(': lev*=10;
            break;
            case ')': lev/=10;
            break;
        }
    }

    if (no==-1)
    {
        while (strf[0]=='(') strf=strf.substr(1,strf.size()-2);
        PNode->val=strf;
    }
    else
    {
        if (prior>=10)
        {
            strf=strf.substr(1,strf.size()-2);
            --no;
        }
        PNode->val=strf[no];
        PNode->right = new Rast;
        setRNode(strf.substr(no+1,strf.size()-no-1),PNode->right);
        if (no!=0)
        {
            PNode->left = new Rast;
            setRNode(strf.substr(0,no),PNode->left);
        }

    }
}

void Chervi::delRNode(Rast* PNode)
{
    if (PNode == 0 ) PNode=RNode;
    if (PNode->left != 0) delRNode(PNode->left);
    if (PNode->right != 0) delRNode(PNode->right);
    delete PNode;
    if (PNode == RNode) RNode =0;
}

double Chervi::CountTree(double t,double v,Rast* PNode)
{
    if (PNode == 0) PNode=RNode;
    if ( !((PNode->left == 0)&&(PNode->val == "-")) )
        switch (PNode->val[0])
        {
            case '*': return CountTree(t,v,PNode->left)*CountTree(t,v,PNode->right);
            case '/': return CountTree(t,v,PNode->left)/CountTree(t,v,PNode->right);
            case '+': return CountTree(t,v,PNode->left)+CountTree(t,v,PNode->right);
            case '^': return pow(CountTree(t,v,PNode->left),CountTree(t,v,PNode->right));
            case '-': return CountTree(t,v,PNode->left)-CountTree(t,v,PNode->right);
            case 't': return t;
            case 'v': return v;
            default : return StrToDouble(PNode->val);
        }
    else return (-1)*CountTree(t,v,PNode->right);

}

int Chervi::DoAltMnog()
{
    for (int j=0;j<Datas[0].size();++j)
        for (int i=0; i<Datas.size();++i)
            if (Datas[i][j]==1)
                if (checkMs(i,j))
                {
                    int ci=AddItog(-1,i,j);
                    FAllSosedsAlt(ci, i,j);
                };
    emit LevelDone(3);
    return 0;
}

int Chervi::FAllSosedsAlt(int ci, int rmi, int rmj){
    Rs t_mR={getmxRtAlt(rmi), getmxRvAlt(rmi)};
    indxs sosed=FSosedAlt(rmi,rmj,t_mR);
    int res=0;
    while (sosed.Mi!=-1){
        AddItog(ci,sosed.Mi,sosed.Mj);
        rmi=sosed.Mi;
        rmj=sosed.Mj;
        t_mR.Rt=getmxRtAlt(rmi);
        t_mR.Rv=getmxRvAlt(rmi);
        ++res;
        sosed=FSosedAlt(rmi,rmj,t_mR);
    };
    return res;
};

indxs Chervi::FSosedAlt(int fmi, int fmj, Rs maxR)
{
    indxs res = {-1, -1};
    double BestWin=-1;
    for (int i=0;(i<(maxR.Rv/2));++i)
        for (int j=1;(j<maxR.Rt)&&((fmj+j)<Datas[0].size());++j)
        {
            if (((fmi+i)<Datas.size())&&(Datas[fmi+i][fmj+j]==1)
              &&(checkMs(fmi+i,fmj+j))
              &&(BlizhSosedSlevaAlt(fmi+i, fmj+j, fmi, fmj))
              &&((CountTree(j,i)<BestWin)||(BestWin==-1))  )
            {
                res.Mi=fmi+i;
                res.Mj=fmj+j;
                BestWin=CountTree(j,i);
            };
            if ((i!=0)&&((fmi-i)>=0)&&(Datas[fmi-i][fmj+j]==1)
              &&checkMs(fmi-i,fmj+j)&&(BlizhSosedSlevaAlt(fmi-i, fmj+j, fmi, fmj))
              &&((CountTree(j,i)<BestWin)||(BestWin==-1)) )
            {
                res.Mi=fmi-i;
                res.Mj=fmj+j;
                BestWin=CountTree(j,i);
            };
        };
    return res;
};

int Chervi::BlizhSosedSlevaAlt(int mi, int mj, int bmi, int bmj){
    int res=0;
    indxs sosed={-1, -1};
    double BestWin=-1;
    for (int i=0;i<(getmxRvAlt(mi)/2);++i)
        for (int j=1;(j<getmxRtAlt(mi))&&((mj-j)>=0);++j)
        {
            if ( ((mi+i)<Datas.size())&&(Datas[mi+i][mj-j]==1)
//              &&((Findci(mi+i,mj-j)==-1)||(Findci(mi+i,mj-j)==Findci(bmi,bmj)))
              &&((CountTree(j,i)<BestWin)||(BestWin==-1))   )
            {
                sosed.Mi=mi+i;
                sosed.Mj=mj-j;
                BestWin=CountTree(j,i);
            };
            if ( (i!=0)&&((mi-i)>=0)&&(Datas[mi-i][mj-j]==1)
//              &&( (Findci(mi-i,mj-j)==-1)||(Findci(mi-i,mj-j)==Findci(bmi,bmj)) )
              &&((CountTree(j,i)<BestWin)||(BestWin==-1)) )
            {
                sosed.Mi=mi-i;
                sosed.Mj=mj-j;
                BestWin=CountTree(j,i);
            };
        };
    if ( (sosed.Mi==bmi)&&(sosed.Mj==bmj) ) res=1;
    else res=0;

    return res;

};

double Chervi::getmxRtAlt(int i){
    double res=0;
    if ( (i>=0)&&(i<mxRvRts.size())) res=mxRvRts[i].Rt;
    return res;
};

double Chervi::getmxRvAlt(int i){
    double res=0;
    if ( (i>=0)&&(i<mxRvRts.size())) res=mxRvRts[i].Rv;
    return res;
};

QVector < QVector < double > > Chervi::LoadMtrx(QString fname)
{
    QVector < QVector < double > > res(0);
    QFile file(fname);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);
    while (!in.atEnd())
    {
        QStringList strs=ClrFstr(in.readLine()).split(" ");
        QVector < double > tmps(0);
        for (int i=0;i<strs.size();++i)
            tmps.push_back(strs.at(i).toDouble());
        if (tmps.size()>0) res.push_back(tmps);
    }
    file.close();
    return res;
}

#ifndef GRAFICS_H
#define GRAFICS_H

#include <QWidget>
#include <QPainter>

namespace Ui {
    class grafics;
}

class grafics : public QWidget {
    Q_OBJECT
public:
    grafics(QWidget *parent = 0);
    grafics(QPixmap graf, QWidget *parent = 0);
    grafics(int test, QWidget *parent = 0);
    ~grafics();

protected:
    void changeEvent(QEvent *e);
    void paintEvent(QPaintEvent *event);

private:
    Ui::grafics *ui;
    QPixmap *Mgraf;
};

#endif // GRAFICS_H

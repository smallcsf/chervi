#include "grafics.h"
#include "ui_grafics.h"

grafics::grafics(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::grafics)
{
    ui->setupUi(this);
};

grafics::grafics(int test, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::grafics)
{
    ui->setupUi(this);
    Mgraf = new QPixmap(300,300);
    Mgraf->fill(QColor(0,255,0));
    QPainter pp(Mgraf);
        pp.drawText(10,10,"HIIIIIIIIIIIII_____"+QString::number(test)+"____!");
    pp.end();

};

grafics::grafics(QPixmap graf,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::grafics)
{
    ui->setupUi(this);
    Mgraf = new QPixmap(graf.size());
    *Mgraf=graf;
};

grafics::~grafics()
{
    delete ui;
}

void grafics::paintEvent(QPaintEvent *event){
    QPainter pain(this);
        if (!Mgraf->isNull()) pain.drawPixmap(0,0,*Mgraf);
    pain.end();
};

void grafics::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

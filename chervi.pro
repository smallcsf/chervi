#-------------------------------------------------
#
# Project created by QtCreator 2012-03-16T20:30:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = chervi
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    grafics.cpp \
    chervi.cpp

HEADERS  += mainwindow.h \
    grafics.h \
    chervi.h

FORMS    += mainwindow.ui \
    grafics.ui

OTHER_FILES += \
    chervi.pro.user
